from allennlp.predictors.predictor import Predictor
from argparse import ArgumentParser
import pandas as pd
import numpy as np
import random
import regex
import sys
import re


def empty_tags(split_sentence):
    if split_sentence == '':
        return False
    return True

# moving information around
def move_error(predictor, sentence):
    parsing = predictor.predict(sentence=sentence)
    # list of POS tags in the sentence
    tags = parsing['pos_tags']
    print(tags)
    # 2 random integers are picked (which word to pick, and where to put it)
    error_indexes = np.random.randint(len(tags), size=2)
    sentence = regex.sub(r'(?=\p{P})', r' ', sentence)
    sentence = sentence.split(" ")
    sentence = list(filter(empty_tags, sentence))
    # check that I'm not moving punctuaction around
    while regex.search(r'(?=\p{P})', tags[error_indexes[0]]):
        error_indexes = np.random.randint(len(tags), size=2)
    sentence.insert(error_indexes[1], sentence.pop(error_indexes[0]))
    # putting the sentence back together
    return " ".join(sentence)


def apply_error(predictor, sentence, err):
    print("SEARCHING for error: {}".format(err))
    parsing = predictor.predict(sentence=sentence)
    # list of POS tags in the sentence
    tags = parsing['pos_tags']
    print(tags)
    error_index = np.random.randint(len(tags))
    sentence = regex.sub(r'(?=\p{P})', r' ', sentence)
    sentence = sentence.split(" ")
    sentence = list(filter(empty_tags, sentence))
    look_for = None
    if err == "other":
        # check that the word is not a subject, an object, or a verb
        count = 0
        while (re.match("NN", tags[error_index])) or (tags[error_index] == "CD") or (re.match("VB", tags[error_index])) or (regex.search(r'(?=\p{P})', tags[error_index])):
            error_index = np.random.randint(0, len(tags))
            count += 1
            if count > len(tags):
                return None
        look_for = [tags[error_index]]
    else:
        # find the word we need to work with
        look_for = re.findall(r'(?<=_).*', err)
    # dealing with more than 1 match
    reg = r""+re.escape(look_for[0])
    reg = re.compile(reg)
    # retrieve tag and do something
    values = []
    for val in tags:
        if re.match(reg, val):
            values.append(sentence[tags.index(val)])

    print("Values: ", values)
    if not values:
        return None
    to_apply = np.random.randint(0, len(values))
    # check whether to introduce or remove the word
    to_do = None
    if err != "other":
        to_do = re.match(".+?(?=_)", err).group(0)

    if to_do is None:
        to_do = random.choice(["int", "rem"])

    if to_do == "int":
        sentence.insert(error_index, values[to_apply])
    elif to_do == "rem":
        count = 0
        for word in sentence:
            if word == values[to_apply]:
                if count == to_apply:
                    sentence.pop(sentence.index(word))
                count += 1
    # put the sentence back together before returning it
    return " ".join(sentence)


if __name__ == '__main__':
    ap = ArgumentParser()
    # list of files in input
    ap.add_argument('-i', '--input_files', help='files with human-generated sentences', required=True)
    ap.add_argument('-one', help='number of sentences rated as a 1')
    ap.add_argument('-two', help='number of sentences rated as a 2')
    ap.add_argument('-three', help='number of sentences rated as a 3')
    ap.add_argument('-four', help='number of sentences rated as a 4')
    ap.add_argument('-five', help='number of sentences rated as a 5')
    # ap.add_argument('-six', help='number of sentences rated as a 6')

    args = ap.parse_args()
    read_file = pd.read_csv(args.input_files, sep='\t', header=0)
    max_ones = int(args.one)
    max_twos = int(args.two)
    max_threes = int(args.three)
    max_fours = int(args.four)
    max_fives = int(args.five)
    # max_sixs = args.six

    errors_type = {'int_NN': (0, 1, 1),
                   'int_CD': (0, 1, 1),
                   'rem_NN': (2, 1.5, 1.5),
                   'rem_CD': (2, 1.5, 1.5),
                   'int_VB': (0, 1, 1),
                   'rem_VB': (1, 2, 1.5),
                   'other': (0, 0.5, 0.5),
                   'move': (1, 1, 1)}

    total = max_ones+max_twos+max_threes+max_fours+max_fives
    # which type of error? 2 choices: syntactic / dependency (1/2)
    errors = [0, 1]  # 0=syntactic, 1=dependency

    # TODO: read all files and store them in a dataframe (?)
    total_sentences = read_file['sentence']
    sentence_ind = 0

    syntactic_parse = Predictor.from_path("https://s3-us-west-2.amazonaws.com/allennlp/models/elmo-constituency-parser-2018.03.14.tar.gz")

    max_rating = 6

    new_data = pd.DataFrame(columns=['dataset', 'system', 'is_real', 'data_representation', 'output', 'informativeness', 'naturalness', 'quality'])


    while total > 0:
        # deciding on the type of error to introduce
        error_intr = np.random.choice(errors, p=[0.3, 0.7])
        curr_sent = total_sentences[sentence_ind]
        print('--------------------------------')
        print('BASE sentence: ', curr_sent)
        curr_rating = [6, 6, 6]
        # infinitely cycle through the list – restoring the index
        sentence_ind = (sentence_ind + 1) % len(total_sentences)
        check_errors = {curr_sent: curr_rating}
        to_achieve_rat = 0

        if max_fives > 0:
            to_achieve_rat = 5
            break
        elif max_fours > 0:
            to_achieve_rat = 4
            break
        elif max_threes > 0:
            to_achieve_rat = 3
            break
        elif max_twos > 0:
            to_achieve_rat = 2
            break
        else:
            to_achieve_rat = 1

        errors_needed = max_rating - to_achieve_rat
        print("Applying {} errors!".format(errors_needed))
        while errors_needed > 0:
            err_type, err_rate = random.choice(list(errors_type.items()))
            if err_rate[0] <= errors_needed:  # 6(max rating)-5(what we want to achieve)
                # save sentence to dict
                new_data.append({'dataset': '<to_change>', 'system': '<to_change>', 'is_real': '<to_change>', 'data_representation': '<to_change>', 'output': curr_sent, 'informativeness': curr_rating[0], 'naturalness': curr_rating[1], 'quality': curr_rating[2]}, ignore_index=True)
                # apply error
                if err_type == 'move':
                    temp_sent = move_error(syntactic_parse, curr_sent)
                else:
                    temp_sent = apply_error(syntactic_parse, curr_sent, err_type)
                # apply new rating
                if temp_sent != None:
                    curr_sent = temp_sent
                    print("SENTENCE HAS BEEN CHANGED: ", curr_sent)
                    if curr_sent in check_errors:
                        curr_rating = check_errors[curr_sent]
                        errors_needed = curr_rating[2]-to_achieve_rat
                    else:
                        # gives the possibility to introduce many errors but never end up with a zero/negative rating
                        curr_rating = np.subtract(curr_rating, err_rate)
                        for i, rates in enumerate(curr_rating):
                            if rates < 1:
                                curr_rating[i] = 1
                        print(curr_rating)
                        check_errors[curr_sent] = curr_rating
                        # introducing errors with different scores and ratings
                        errors_needed -= err_rate[0]
                if errors_needed == 0:
                    # write new sentence with new ratig
                    new_data.append({'dataset': '<to_change>', 'system': '<to_change>', 'is_real': '<to_change>', 'mr': '<to_change>', 'output': curr_sent, 'informativeness': curr_rating[0], 'naturalness': curr_rating[1], 'quality': curr_rating[2]}, ignore_index=True)
        total -= 1