from argparse import ArgumentParser
from bert_score import score, plot_example
import pandas as pd
import transformers
import logging
import sys
import os


if __name__ == '__main__':
    transformers.tokenization_utils.logger.setLevel(logging.ERROR)
    transformers.configuration_utils.logger.setLevel(logging.ERROR)
    transformers.modeling_utils.logger.setLevel(logging.ERROR)

    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-c', '--candidate_files', nargs='+', help='file containing the candidate sentences', required=True)
    ap.add_argument('-m', '--MRs', nargs='+', help='file containing the meaning representations', required=True)
    ap.add_argument('-r', '--reference_file', help='file containing the reference sentences', required=True)

    args = ap.parse_args()

    with open(args.reference_file) as f:
        refs = [line.strip() for line in f]

    mean_repr = args.MRs
    dets = []  # pd.DataFrame(columns=['system', 'mr'])
    if len(mean_repr) > 1:
        for s_file in mean_repr:
            f = pd.read_csv(s_file)
            dets.append(f[['system', 'mr']])
            # dets = dets.append(f[['system', 'mr']].copy())
    else:
        f = pd.read_csv(mean_repr)
        dets = f[['system', 'mr']].copy()

    dets = pd.concat(dets, axis=0, ignore_index=True)

    cands_mul = args.candidate_files
    count = 0
    frame = pd.DataFrame(columns=['system', 'mr', 'candidate', 'P', 'R', 'F1'])
    for cands in cands_mul:
        filename = os.path.basename(cands)
        filename = filename.split('_')[1]
        filename = filename.split('.')[0]
        print('\nEvaluating {} system [{}]'.format(filename, count))

        with open(cands) as f:
            cand = [line.strip() for line in f]
        P, R, F1 = score(cand, refs, lang='en', verbose=True)


        #print(dets.iloc[count]['system'], dets.iloc[count]['mr'])
        # print(f"System level precision score: {P.mean():.3f}")
        # print(f"System level recall score: {R.mean():.3f}")
        # print(f"System level F1 score: {F1.mean():.3f}")
        #final_name = 'res_' + filename + 'txt'
        for i in range(len(cand)):
            frame = frame.append({'system': dets.iloc[i+(len(cand)*count)]['system'], 'mr': dets.iloc[i+(len(cand)*count)]['mr'], 'candidate': cand[i], 'P': float(P[i]), 'R': float(R[i]), 'F1': float(F1[i])}, ignore_index=True)
        count += 1

    write = frame.to_csv('results.tsv', sep='\t', index=False)

    # plot_example(cands[0], refs[0], lang="en", fname='figure.png')