from argparse import ArgumentParser
import pandas as pd
import math
import sys

if __name__ == '__main__':
    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-c', '--results_file', help='file containing BERT results', required=True)
    ap.add_argument('-r', '--human_ratings', help='file containing human ratings', required=True)

    args = ap.parse_args()
    results = pd.read_csv(args.results_file, sep='\t', header=0)
    ratings = pd.read_csv(args.human_ratings, header=0)

    #human ratings
    mr = ratings['mr']
    sys_1, out_1, quality1 = (ratings['sys1'], ratings['ref1'], ratings['quality1'])
    sys_2, out_2, quality2 = (ratings['sys2'], ratings['ref2'], ratings['quality2'])
    sys_3, out_3, quality3 = (ratings['sys3'], ratings['ref3'], ratings['quality3'])
    sys_4, out_4, quality4 = (ratings['sys4'], ratings['ref4'], ratings['quality4'])
    sys_5, out_5, quality5 = (ratings['sys5'], ratings['ref5'], ratings['quality5'])

    mr_bert = results['mr']
    candidate = results['candidate']
    # parsing BERT scores to find matching ones
    appear = []
    """
        check if MR in human ratings matches the one in the BERT results file;
        check if the system & the candidate sentence match as well;
        if it does, add the F1 score
        [mr,qual1-qual5,ref1-ref5,sys1-sys5]
    """
    results.set_index('system', inplace=True)
    agreed = 0
    out_of = 0
    for _line in ratings.index:
        human_r = [(sys_1[_line], quality1[_line]), (sys_2[_line], quality2[_line]), (sys_3[_line], quality3[_line]), (sys_4[_line], quality4[_line]), (sys_5[_line], quality5[_line])]  # human evaluations
        res_ = []  # store each line results here
        # print('Looking for {}'.format(mr[_line]))
        for val in human_r:
            # select rows with matching system
            try:
                res = results.loc[val[0]]
                print(res)
                res = res.loc[res['mr'] == mr[_line]]
                res = res.loc[res['candidate'] == out_1[_line]]
                max_val = res['F1'].max()
                if not math.isnan(max_val):
                    res_.append((val[0], max_val))
            except:
                print("Not found!")
                pass


        if len(res_) > 0:
            human_r = sorted(human_r, key=lambda x: x[1], reverse=True)
            res_ = sorted(res_, key=lambda x: x[1], reverse=True)
            y = True
            for i in range(len(res_)):
                if human_r[i][0] != res_[i][0]:
                    y = False
                    break
            if y:
                agreed += 1
            out_of += 1


    print('Agreed on {}/{} rankings'.format(agreed, out_of))
        # if len(res) > 0:
        #     appear.append(res)
        #     print('Foud something: {}\n Length: {}'.format(res, len(res)))




            # not working
            # human_r = human_r.sort(key=lambda x: x[1], reverse=True)
            # res = res.sort(key=lambda x: x[1], reverse=True)
        # check whether ratings correspond
    # check ratings
