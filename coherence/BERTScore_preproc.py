from argparse import ArgumentParser
#import transformers
import pandas as pd
import sys
import os
#import logging

if __name__ == '__main__':

    ap = ArgumentParser()
    ap.add_argument('-c', '--candidate_file', help='file containing the candidate sentences', required=True)
    ap.add_argument('-r', '--reference_file', help='file containing the reference sentences', required=True)

    args = ap.parse_args()
    # .tsv file
    candidate = pd.read_csv(args.candidate_file, sep='\t', header=0)
    base = os.path.basename(args.candidate_file)
    file_name = base.split('.')[0]
    # .csv file
    reference = pd.read_csv(args.reference_file, header=0)

    fin_candidates = []
    fin_references = []
    # meaning representation + system
    fin_ms = []

    to_search = reference['mr']
    dets = pd.DataFrame(columns=['system', 'mr'])
    search_sent = reference['ref']
    cand = candidate['MR']
    cand_sent = candidate['output']
    for mr, line in zip(to_search, search_sent):
        for cand_mr, cand_line in zip(cand, cand_sent):
            # found candidate sentence corrisponding to reference
            if mr == cand_mr:
                # save both sentences to separate files
                fin_references.append(line)
                fin_candidates.append(cand_line)
                dets = dets.append({'system': file_name, 'mr': cand_mr}, ignore_index=True)
                break
        #print(fin_candidates)


    # final_1 = ref_file.to_csv(path_or_buf='references_BERT.csv')
    # name_cand = 'candidates_'+file_name+'.csv'
    # final_2 = cand_file.to_csv(path_or_buf=name_cand)

    with open('cand_files/references_BERT.txt', 'w') as ref_file:
        for ref in fin_references:
            ref_file.write("%s\n" % ref)

    name_cand = 'cand_files/candidates_'+file_name+'.txt'
    with open(name_cand, 'w') as cand_file:
        for row in fin_candidates:
            cand_file.write("%s\n" % row)
        # cand_file.writelines(fin_candidates)
    details = 'cand_files/details_' + file_name + '.csv'
    fin = dets.to_csv(details, index=False)
