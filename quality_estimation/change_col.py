from argparse import ArgumentParser
import pandas as pd

ap = ArgumentParser()
ap.add_argument('input_file', help='file with line to be changed')

args = ap.parse_args()
in_file = pd.read_csv(args.input_file, sep='\t')

in_file.loc[in_file['error_type'] == 2, 'error_type'] = 1
in_file.loc[in_file['error_type'] == 3, 'error_type'] = 1
in_file.loc[in_file['error_type'] == 6, 'error_type'] = 1
in_file.loc[in_file['error_type'] == 5, 'error_type'] = 2
in_file.loc[in_file['error_type'] == 7, 'error_type'] = 2
in_file.loc[in_file['error_type'] == 4, 'error_type'] = 3

new = pd.DataFrame(in_file)
new = new.to_csv('samples_annotated_newc.tsv', sep='\t', index=False)