# script to annotate the E2E data based on the type of errors generated from the systems
from argparse import ArgumentParser
import pandas as pd
import sys

ap = ArgumentParser()
ap.add_argument('-i', '--input_file', help='file that needs to be annotated', required=True)
ap.add_argument('-a', '--annotated_file', help='file that has been partly annotated')
args = ap.parse_args()

input_file = pd.read_csv(args.input_file, sep='\t')
annotated_file = args.annotated_file
new_data = None
file_len = len(input_file.index)
annotated_file_len = None
try:
    annotated_file = pd.read_csv(annotated_file, sep='\t')
    new_data = annotated_file
    annotated_file_len = len(annotated_file.index)
except:
    new_data = pd.DataFrame(columns=['system', 'MR', 'output', 'error_type'])


for index, row in input_file.iterrows():
    # make sure the user didn't annotate the utterance before --> if error type is not null
    if (annotated_file_len is not None) and (index < annotated_file_len):
        continue

    print('Annotating sentence {} out of {}.'.format(index+1, file_len))

    print('\n')
    print('System: {} \n'.format(row.loc['system']))
    print('MR: {} \n'.format(row.loc['MR']))
    print('Sentence: {} \n'.format(row.loc['output']))
    print('ANNOTATE')
    print('Type of error:')
    print('1 - Disfluency, 2 - Information problems, 3 - No errors\n')

    # capture ctrl+c from the user and save the file
    try:
        user_input = None
        while True:
            user_input = input('')
            print('\n')
            # check that input is integer and in the correct range
            user_input = user_input.split(',')
            #print(user_input)
            # not handling non integers in the list type
            if isinstance(user_input, list):
                val_input = []
                for value in user_input:
                    try:
                        value = int(value)
                        if 1 <= value <= 3:
                            val_input.append(value)
                        else:
                            print('Number not in the correct range! Try again:')
                            continue
                    except ValueError:
                        print('Not an integer! Try again:')
                        continue
                new_data = new_data.append({'system': input_file.iloc[index, 0], 'MR': input_file.iloc[index, 1], 'output': input_file.iloc[index, 2], 'error_type': val_input}, ignore_index=True)
                break
            else:
                try:
                    val = int(user_input)
                    if 1 <= val <= 3:
                        new_data = new_data.append({'system': input_file.iloc[index, 0], 'MR': input_file.iloc[index, 1], 'output': input_file.iloc[index, 2], 'error_type': user_input}, ignore_index=True)
                        break
                    else:
                        print('Number not in the correct range! Try again:')
                        continue
                    break
                except ValueError:
                    print('Not an integer! Try again:')
                    continue
    except KeyboardInterrupt:
        print('\nI\'m saving on file..')
        final = new_data.to_csv(path_or_buf='samples_ann_total.tsv', sep='\t', index=False)
        sys.exit()