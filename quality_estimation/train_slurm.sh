#!/bin/bash

#SBATCH --nodes 1
#SBATCH --partition=amd-longq
#SBATCH --mail-user=ks85@hw.ac.uk
#SBATCH --mail-type=ALL
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=16

# do some stuff to setup the environment
CUDA_VERSION=cuda90
CUDA_VERSION_LONG=9.0.176
CUDNN_VERSION=7.0
#module purge
module load shared
module load $CUDA_VERSION/blas/$CUDA_VERSION_LONG $CUDA_VERSION/fft/$CUDA_VERSION_LONG $CUDA_VERSION/nsight/$CUDA_VERSION_LONG $CUDA_VERSION/profiler/$CUDA_VERSION_LONG $CUDA_VERSION/toolkit/$CUDA_VERSION_LONG cudnn/$CUDNN_VERSION

# execute application (read in arguments from command line)
source /home/ksevegnani/qe/bin/activate && cd /home/ksevegnani/ratpred && sh ./noref.sh

# exit
exit 0
