#import xmltodict
from argparse import ArgumentParser
import pandas as pd
import datetime
import boto3
import sys


region_name = 'us-east-1'
aws_access_key_id = 'AKIAJOEJCMPQLKKNUGTA'
aws_secret_access_key = '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'

endpoint_url = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'

# Uncomment this line to use in production
# endpoint_url = 'https://mturk-requester.us-east-1.amazonaws.com'

client = boto3.client(
    'mturk',
    endpoint_url=endpoint_url,
    region_name=region_name,
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key,
)

# This will return $10,000.00 in the MTurk Developer Sandbox
print(client.get_account_balance()['AvailableBalance'])


ap = ArgumentParser()
ap.add_argument('input_file', type=str, help='file that contains all the sentences that need to be parsed')
ap.add_argument('-d', type=bool, default=False, help='delete all studies')
args = ap.parse_args()
delete = args.d
in_file = pd.read_csv(args.input_file, sep=',')


qual_types = client.list_qualification_types(MustBeRequestable=False)
for type_ in qual_types['QualificationTypes']:
    print(type_)
    qual_id = type_['QualificationTypeId']
    del_ = client.delete_qualification_type(QualificationTypeId=qual_id)


# Delete HITs that have been created
if delete:
    hits = client.list_hits(MaxResults=100)
    for hit in hits['HITs']:
        hit_id = hit['HITId']
        response = client.update_expiration_for_hit(
            HITId=hit['HITId'],
            ExpireAt=datetime.datetime(2015, 1, 1)
        )
        response = client.delete_hit(
            HITId=hit['HITId']
        )

        print('Deleted HIT ', hit_id)

# create new study
else:
    question = open('questions.xml', mode='r').read()
    test_questions = open('test_quests.xml', mode='r').read()
    answer_questions = open('answ_test.xml', mode='r').read()

    qualification = client.create_qualification_type(
        Name='Quality Estimation for Automatic Language Generation',
        Description='Qualification test in order to take part in the HIT.',
        Keywords='language, English, quality, ratings',
        QualificationTypeStatus='Active',
        Test=test_questions,
        AnswerKey=answer_questions,
        TestDurationInSeconds=600,
        )

    print(qualification['QualificationType']['QualificationTypeId'])

    check = ["${mr}", "${sys_ref}"]
    for index, utt in in_file.iterrows():
        question = question.replace("${mr}", utt["mr"])
        question = question.replace("${sys_ref}", utt["sys_ref"])

    new_hit = client.create_hit(
        Title='Quality Estimation for Automatic Language Generation',
        Description='The purpose of this study is to evaluate the quality of English sentences that have been produced by automated systems. This will help us understand where automated systems fail the most when producing natural language.',
        Keywords='language, English, quality, ratings',
        Reward='0.01',
        MaxAssignments=3,
        LifetimeInSeconds=172800,  # duration: 48 hours
        AssignmentDurationInSeconds=1200,
        AutoApprovalDelayInSeconds=14400,
        Question=question,
        QualificationRequirements=[{'QualificationTypeId': qualification['QualificationType']['QualificationTypeId'],
                                       'Comparator': 'EqualTo',
                                       'IntegerValues':[100]}]
    )

    print("A new HIT has been created. You can preview it here:")
    print("https://workersandbox.mturk.com/mturk/preview?groupId=" + new_hit['HIT']['HITGroupId'])

    # print("https://worker.mturk.com/mturk/preview?groupId=" + new_hit['HIT']['HITGroupId'])
    print("HITID = " + new_hit['HIT']['HITId'] + " (Use to Get Results)")


# Remember to modify the URL above when you're publishing
# HITs to the live marketplace.
# Use: https://worker.mturk.com/mturk/preview?groupId=