from argparse import ArgumentParser
from rouge import Rouge
import numpy as np
import json

if __name__ == '__main__':
    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-i', '--input_file', help='file containing Topical-Chat conversations', required=True)
    ap.add_argument('-r', '--reading_set', help='file containing Topical-Chat reading sets [Washington post article, wikilead, Reddit fun facts]', required=True)
    args = ap.parse_args()
    rouge = Rouge()

    avg_rouge_wiki1 = []
    avg_rouge_wiki2 = []
    avg_rouge_wikil = []
    avg_rouge_redd1 = []
    avg_rouge_redd2 = []
    avg_rouge_reddl = []
    with open(args.input_file, 'r') as conv:
        with open(args.reading_set, 'r') as read_set:
            convs = json.load(conv)
            reads = json.load(read_set)
            for k,v in convs.items():
                #print('Processing conversation #', k)
                for message in v['content']:
                    knwdg_src = message['knowledge_source']
                    if (len(knwdg_src) == 1) and ('Personal Knowledge' in knwdg_src):
                        continue
                    else:
                        sent = message['message']
                        agent = message['agent']
                        for knw in knwdg_src:
                            if (knw == "FS1") or (knw == "FS2") or (knw == "FS3"):
                                if 'shortened_wiki_lead_section' in reads[k][agent][knw]: 
                                    wiki = reads[k][agent][knw]['shortened_wiki_lead_section']
                                else:
                                    wiki = reads[k][agent][knw]['summarized_wiki_lead_section']
                                # list of fun facts
                                fun_fact = reads[k][agent][knw]['fun_facts']
                                separator = " "
                                fun_fact = separator.join(fun_fact)
                                """ 
                                    [{"rouge-1": {"f": 0.49411764217577864,
                                                  "p": 0.5833333333333334,
                                                  "r": 0.42857142857142855},
                                      "rouge-2": {"f": 0.23423422957552154,
                                                  "p": 0.3170731707317073,
                                                  "r": 0.18571428571428572},
                                      "rouge-l": {"f": 0.42751590030718895,
                                                  "p": 0.5277777777777778,
                                                  "r": 0.3877551020408163}}]
                                """
                                try:
                                    rouge_wiki = rouge.get_scores(sent, wiki)[0]
                                    rouge_reddit = rouge.get_scores(sent, fun_fact)[0]

                                    avg_rouge_wiki1.append(rouge_wiki["rouge-1"]["f"])
                                    avg_rouge_wiki2.append(rouge_wiki["rouge-2"]["f"])
                                    avg_rouge_wikil.append(rouge_wiki["rouge-l"]["f"])
                                    avg_rouge_redd1.append(rouge_reddit["rouge-1"]["f"])
                                    avg_rouge_redd2.append(rouge_reddit["rouge-2"]["f"])
                                    avg_rouge_reddl.append(rouge_reddit["rouge-l"]["f"])
                                except:
                                    print('Something went wrong:\nsent: {}\twiki: {}\treddit: {}'.format(sent, wiki, fun_fact))

                                
    avg_rouge_wiki1 = np.array(avg_rouge_wiki1)
    avg_rouge_wiki2 = np.array(avg_rouge_wiki2)
    avg_rouge_wikil = np.array(avg_rouge_wikil)
    avg_rouge_redd1 = np.array(avg_rouge_redd1)
    avg_rouge_redd2 = np.array(avg_rouge_redd2)
    avg_rouge_reddl = np.array(avg_rouge_reddl)

    avg_rouge_wiki1 = np.mean(avg_rouge_wiki1)
    avg_rouge_wiki2 = np.mean(avg_rouge_wiki2)
    avg_rouge_wikil = np.mean(avg_rouge_wikil)
    avg_rouge_redd1 = np.mean(avg_rouge_redd1)
    avg_rouge_redd2 = np.mean(avg_rouge_redd2)
    avg_rouge_reddl = np.mean(avg_rouge_reddl)


    print('Results ROUGE-1:\n\tWiki leads: {}\tReddit: {}\n'.format(avg_rouge_wiki1, avg_rouge_redd1))
    print('Results ROUGE-2:\n\tWiki leads: {}\tReddit: {}\n'.format(avg_rouge_wiki2, avg_rouge_redd2))
    print('Results ROUGE-l:\n\tWiki leads: {}\tReddit: {}\n'.format(avg_rouge_wikil, avg_rouge_reddl))