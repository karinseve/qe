"""
    INPUT: PersonaChat file
    OUTPUT: csv file with previous sentence, current sentence, and coherence score for current sentence

    Possible annotations:
        1 - Coherent
        2 - Not-Coherent
        3 - None
"""
from argparse import ArgumentParser
import pandas as pd
import csv
import sys

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('input_file', help='PersonaChat file as input')
    ap.add_argument('-o', '--annotated_file', help='csv output file')
    args = ap.parse_args()

    input_file = args.input_file
    annotated_file = args.annotated_file

    index = None
    annotated_file_len = 0
    exists = None
    #new_data = None
    try:
        f = open(annotated_file)
        exists = True
        annotated_file_ = pd.read_csv(annotated_file, sep='\t')
        #new_data = annotated_file
        annotated_file_len = len(annotated_file_.index)
    except IOError:
        exists = False
    new_data = pd.DataFrame(columns=['Previous', 'Current', 'Coherence'])

    with open(input_file, 'r') as in_file:
        count = 0
        previous = None
        current = None
        dict_ = {}
        for line in in_file:
            line = line.rstrip()
            current = line
            line = line.split('\t')            
            if count == 0:
                previous = current
                current = current
            count += 1            
            if (count < annotated_file_len) or ('persona' in current):
                #previous = line[0]
                previous = current
                print('skipping')
                #print('count: {}, length: {}'.format(count, annotated_file_len))
                #print('Previous: {} - Current: {}'.format(previous, current))
                continue
            previous = line[0]
            current = line[1]
            print('Annotating sentence {}.'.format(count))
            print('\tSENTENCE 1: {}\n\tSENTENCE 2: {}'.format(previous, current))
            print('-----------------------------------------')
            print('1 - Coherent\t2 - Non-coherent\t3 - Neither')
                # capture ctrl+c from the user and save the file
            user_input = None
            while True:
                try:
                    user_input = input('')
                    print('\n')
                    # check that input is integer and in the correct range
                    try:
                        value = int(user_input)
                        if 0 < value < 4:
                            #store it
                            new_data = new_data.append({'Previous': previous, 'Current': current, 'Coherence': value}, ignore_index=True)
                            previous = current
                            break
                        else:
                            print("Not in the correct range")
                            continue
                        break
                    except ValueError:
                        print('Not the correct value!')
                        continue
                except KeyboardInterrupt:
                    print('\nI\'m saving on file..')
                    if exists:
                        print('Appending')
                        final = new_data.to_csv(annotated_file, sep='\t', mode='a', index=False, header=False)
                    else:
                        final = new_data.to_csv(annotated_file, sep='\t', index=False)
                    sys.exit()

                        # not saving on file (saving on different lines)