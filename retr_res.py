from argparse import ArgumentParser
from scipy.stats import chisquare
import pandas as pd
import numpy as np
import statistics
import ast
import sys

def increment_e2e(higher, lower, same, chi_table_qual_e2e, result):
    # print(higher, lower, same)
    if len(higher) > (len(lower)+len(same)):
        chi_table_qual_e2e[result[0]-1, 1] += 1
        return
    elif len(lower) > (len(higher)+len(same)):
        chi_table_qual_e2e[result[0]-1, 0] += 1
        return
    elif len(same) > (len(higher)+len(lower)):
        chi_table_qual_e2e[result[0]-1, 2] += 1
        return
    else:
        high_vals = np.inf
        low_vals = np.inf
        same_vals = np.inf

        if len(higher) > 0:
            high_vals = abs(int(np.sum(higher)/len(higher))-quality_rating)

        if len(lower) > 0:
            low_vals = abs(int(np.sum(lower)/len(lower))-quality_rating)

        if len(same) > 0:
            same_vals = abs(int(np.sum(same)/len(same))-quality_rating)

        # choosing number closer to my actual rating
        choice = np.array([low_vals, high_vals, same_vals])
        # print(choice)
        # print(result[0], np.where(choice == np.amin(choice)))
        chi_table_qual_e2e[result[0]-1, np.where(choice == np.amin(choice))] += 1
    return


if __name__ == '__main__':

    ap = ArgumentParser()
    ap.add_argument('-i', '--input_file', help='annotated file', required=True)
    ap.add_argument('-n', '--naturalness_file', help='annotated file with naturalness rankings')
    ap.add_argument('-q', '--quality_file', help='annotated file with quality rankings', required=True)

    args = ap.parse_args()

    input_file = pd.read_csv(args.input_file, sep='\t', header=0)
    # naturalness = pd.read_csv(args.naturalness_file, sep=',', header=0)
    quality = pd.read_csv(args.quality_file, sep=',', header=0)
    new_file = pd.DataFrame(columns=['system', 'MR', 'output', 'error_type', 'quality', 'naturalness'])
    annotated_file_len = len(input_file.index)
    ones = 0
    twos = 0
    threes = 0
    both = 0
    others = 0
    not_found = 0
    disf_arr_nat = []
    disf_arr_qual = []
    inf_arr_qual = []
    no_err_nat = []
    no_err_qual = []
    both_nat = []
    both_qual = []
    # print(annotated_file_len)

    # COLUMNS: disfluency, information, no_errors, both errors
    chi_table_qual_nem = np.zeros([4, 6])
    chi_table_nat_nem = np.zeros([4, 6])

    chi_table_qual_e2e = np.zeros([4, 3])
    chi_table_nat_e2e = np.zeros([4, 3])

    match = 0
    for line in input_file.index:
        #print(line)
        system = input_file['system'][line]
        mr = input_file['MR'][line]
        output = input_file['output'][line]
        result = input_file['error_type'][line]
        result = ast.literal_eval(result)
        if (len(result) > 1) and (len(result) != len(set(result))):
            # handle this
            print('Need to handle this')
            pass
        elif len(result) > 1:
            both += 1

        for res in result:
            while True:
                if res == 1:
                    ones += 1
                    break
                elif res == 2:
                    twos += 1
                    break
                elif res == 3:
                    threes += 1
                    break
                else:
                    try:
                        res = int(str(res)[0])
                    except:
                        others += 1

        quality_rating = None
        naturalness_rating = None

        # checking for naturalness and quality
        for qual_line in quality.index:
            # E2E data
            # if mr == quality['mr'][qual_line]:
            #     systems = [quality['sys1'][qual_line], quality['sys2'][qual_line], quality['sys3'][qual_line], quality['sys4'][qual_line], quality['sys5'][qual_line]]
            #     refs = [quality['ref1'][qual_line], quality['ref2'][qual_line], quality['ref3'][qual_line], quality['ref4'][qual_line], quality['ref5'][qual_line]]
            #     if (system in systems) and (output in refs):
            #         #print(quality.loc[[qual_line]])
            #         # match += 1
            #         index = systems.index(system)
            #         ratings = [quality['quality1'][qual_line], quality['quality2'][qual_line], quality['quality3'][qual_line], quality['quality4'][qual_line], quality['quality5'][qual_line]]
            #         quality_rating = ratings.pop(index)
            #         if quality_rating > 100:
            #             quality_rating = 100
            #         lower = []
            #         higher = []
            #         same = []
            #         for rat in ratings:
            #             if rat > quality_rating:
            #                 higher.append(rat)
            #             elif rat < quality_rating:
            #                 lower.append(rat)
            #             else:
            #                 same.append(rat)
            #         lower = np.array(lower)
            #         higher = np.array(higher)
            #         same = np.array(same)

            #         # print('Quality')
            #         if len(result) == 1:
            #             increment_e2e(higher, lower, same, chi_table_qual_e2e, result)

            #         else:
            #             #print(result)
            #             increment_e2e(higher, lower, same, chi_table_qual_e2e, [4])

            #         break

            # NEM data [can check for both quality & naturalness in the same file]
            if (mr == quality['MR'][qual_line]) and (output == quality['output'][qual_line]) and (system == quality['system'][qual_line]):
                if len(result) == 1:
                    # print(result[0])
                # print(chi_table_qual_nem[result-1, quality['quality_avg'][qual_line]])
                    chi_table_qual_nem[result[0]-1, quality['quality_avg'][qual_line]-1] += 1
                    chi_table_nat_nem[result[0]-1, quality['naturalness_avg'][qual_line]-1] += 1
                else:
                    # both disfluency + information-related errors on the last row of the matrix
                    chi_table_qual_nem[3, quality['quality_avg'][qual_line]-1] += 1
                    chi_table_nat_nem[3, quality['naturalness_avg'][qual_line]-1] += 1

        # checking for naturalness and quality
        # for nat_line in naturalness.index:
        #     # E2E data
        #     if mr == naturalness['mr'][nat_line]:
        #         systems = [naturalness['sys1'][nat_line], naturalness['sys2'][nat_line], naturalness['sys3'][nat_line], naturalness['sys4'][nat_line], naturalness['sys5'][nat_line]]
        #         refs = [naturalness['ref1'][nat_line], naturalness['ref2'][nat_line], naturalness['ref3'][nat_line], naturalness['ref4'][nat_line], naturalness['ref5'][nat_line]]
        #         if (system in systems) and (output in refs):
        #             index = systems.index(system)
        #             ratings = [naturalness['natur1'][nat_line], naturalness['natur2'][nat_line], naturalness['natur3'][nat_line], naturalness['natur4'][nat_line], naturalness['natur5'][nat_line]]
        #             naturalness_rating = ratings.pop(index)
        #             if naturalness_rating > 100:
        #                 naturalness_rating = 100
        #             lower = []
        #             higher = []
        #             same = []
        #             for rat in ratings:
        #                 if rat > naturalness_rating:
        #                     higher.append(rat)
        #                 elif rat < naturalness_rating:
        #                     lower.append(rat)
        #                 else:
        #                     same.append(rat)
        #             lower = np.array(lower)
        #             higher = np.array(higher)
        #             same = np.array(same)

        #             #print('Naturalness')
        #             if len(result) == 1:
        #                 increment_e2e(higher, lower, same, chi_table_nat_e2e, result)

        #             else:

        #                 increment_e2e(higher, lower, same, chi_table_nat_e2e, [4])
        #             break


        # E2E data

    # print(chi_table_qual_nem)
    # print(chi_table_nat_nem)


    # observed_qual_row = np.sum(chi_table_qual_e2e, axis=1)
    # observed_qual_col = np.sum(chi_table_qual_e2e, axis=0)

    # observed_nat_row = np.sum(chi_table_nat_e2e, axis=1)
    # observed_nat_col = np.sum(chi_table_nat_e2e, axis=0)

    # print(chi_table_qual_e2e)
    # print(observed_qual_row, observed_qual_col)
    # count = 0

    # total_qual = np.sum(observed_qual_row)
    # print(total_qual)

    # observed_val = []
    # expected = []

    # E2E
    # for observed in chi_table_qual_e2e:
    #     observed_val = []
    #     expected = []
    #     for obs in observed:
    #         # expected value for entire table
    #         exp = (observed_qual_col[count%3]*observed_qual_row[int(count/3)])/total_qual

    #         print(obs, exp)

    #         if exp != 0:
    #             observed_val.append(obs)
    #             expected.append(exp)
    #         count += 1




    # print(np.sum(observed_qual_row))
    # print(np.sum(observed_nat_row))

    # quality = chisquare(observed_val, f_exp=expected)
    # print(quality)


    # NEM
    observed_qual_row = np.sum(chi_table_qual_nem, axis=1)
    observed_qual_col = np.sum(chi_table_qual_nem, axis=0)
    count = 0
    total_qual = np.sum(observed_qual_row)

    counter = 0
    for observed in chi_table_qual_nem:
        observed_val = []
        expected = []
        for obs in observed:
            exp = (observed_qual_col[count%6]*observed_qual_row[int(count/6)])/total_qual
            if exp != 0:
                observed_val.append(obs)
                expected.append(exp)
            count += 1
        print(counter, chisquare(observed_val, f_exp=expected))
        counter += 1


    # for observed in chi_table_nat_nem:
    #     for obs in observed:
    #         exp = (observed_nat_col[count%6]*observed_nat_row[int(count/6)])/total_nat
    #         if exp != 0:
    #             observed_val.append(obs)
    #             expected.append(exp)
    #         count += 1

# print(count)
    # print(chi_table_qual_nem)
    # print(observed_qual_row, observed_qual_col)
    # quality = chisquare(observed_val, f_exp=expected)
    # print(quality)


#     if (quality_rating is not None) or (naturalness_rating is not None):
#         #print('Found!')
#         if len(result) > 1:
#             both_qual.append(int(quality_rating*0.06))
#             both_nat.append(int(naturalness_rating*0.06))
#         else:
#             for res in result:
#                 if res == 1:
#                     disf_arr_qual.append(int(quality_rating*0.06))
#                     disf_arr_nat.append(int(naturalness_rating*0.06))
#                 elif res == 2:
#                     inf_arr_qual.append(int(quality_rating*0.06))
#                 elif res == 3:
#                     no_err_qual.append(int(quality_rating*0.06))
#                     no_err_nat.append(int(naturalness_rating*0.06))

#         new_file = new_file.append({'system': system, 'MR': mr, 'output': output, 'error_type': result, 'quality': quality_rating, 'naturalness': naturalness_rating}, ignore_index=True)
#     else:
#         not_found += 1

# print('Disfluency errors: {}; Information errors: {}. Both errors: {}. No errors: {}'.format(ones, twos, both, threes))
# print('Disfluency errors: {}; Information errors: {}. Both errors: {}. No errors: {}'.format((ones/annotated_file_len-both/annotated_file_len), (twos/annotated_file_len-both/annotated_file_len), both/annotated_file_len, threes/annotated_file_len))
# print('Not found {} utterances'.format(not_found))

# print('\n')

# print('Disfluency errors:\nQUALITY\nMinimum {}, Maximum {}, Average {}, Median {}\nNATURALNESS\nMinimum {}, Maximum {}, Average {}, Median {}\n'.format(min(disf_arr_qual), max(disf_arr_qual), statistics.mean(disf_arr_qual), statistics.median(disf_arr_qual), min(disf_arr_nat), max(disf_arr_nat), statistics.mean(disf_arr_nat), statistics.median(disf_arr_nat)))

# print('Information errors:\nQUALITY\nMinimum {}, Maximum {}, Average {}, Median {}\n'.format(min(inf_arr_qual), max(inf_arr_qual), statistics.mean(inf_arr_qual), statistics.median(inf_arr_qual)))

# print('No errors:\nQUALITY\nMinimum {}, Maximum {}, Average {}, Median {}\nNATURALNESS\nMinimum {}, Maximum {}, Average {}, Median {}\n'.format(min(no_err_qual), max(no_err_qual), statistics.mean(no_err_qual), statistics.median(no_err_qual), min(no_err_nat), max(no_err_nat), statistics.mean(no_err_nat), statistics.median(no_err_nat)))

# print('Both errors:\nQUALITY\nMinimum {}, Maximum {}, Average {}, Median {}\nNATURALNESS\nMinimum {}, Maximum {}, Average {}, Median {}\n'.format(min(both_qual), max(both_qual), statistics.mean(both_qual), statistics.median(both_qual), min(both_nat), max(both_nat), statistics.mean(both_nat), statistics.median(both_nat)))

# final = new_file.to_csv(path_or_buf='annotation_samples_compl.tsv', sep='\t', index=False)


