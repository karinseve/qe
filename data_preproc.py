"""
    Script that performs pre-processing of the Google's CCPE movie preferences data.
    ENTITY_NAME = 0
    ENTITY_PREFERENCE = 1
    ENTITY_DESCRIPTION = 2
    ENTITY_OTHER = 3 
    Starting from just the generic preference of the user 

    TODO: entity recognition
"""
from argparse import ArgumentParser
import json
import csv
import sys

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-i', '--input_file', help='file that needs to be tokenized', required=True)
    args = ap.parse_args()

    to_add = []
    with open(args.input_file, 'r') as in_file:
        in_file = json.load(in_file)
        for conversation in in_file:
            for utterance in conversation['utterances']:
                if utterance['speaker'] == 'USER':
                    # text = utterance['text']
                    try:
                        segments = utterance['segments']
                        # dealing with mulitple sentences in one user utterance 
                        for seg in segments:
                            text = seg['text']
                            annotation = seg['annotations'][0]['annotationType']
                            annotation_nmbr = None
                            if annotation == 'ENTITY_NAME':
                                annotation_nmbr = 0
                            elif annotation == 'ENTITY_PREFERENCE':
                                annotation_nmbr = 1
                            elif annotation == 'ENTITY_DESCRIPTION':
                                annotation_nmbr = 2
                            elif annotation == 'ENTITY_OTHER':
                                annotation_nmbr = 3
                            else:
                                annotation_nmbr = 1000
                                print('Found different class: {}'.format(annotation))
                            # print('Appending: {}, {}'.format(annotation_nmbr, text))
                            if annotation_nmbr != 0 and annotation_nmbr != 1000:
                                to_add.append([annotation_nmbr, text])
                    except:
                        print('No annotations!\n{}'.format(utterance))

    with open('converted_file.csv', 'w') as to_write:
        csv_writer = csv.writer(to_write, delimiter=',')
        for ar in to_add:
            csv_writer.writerow([ar[0], ar[1]])