from argparse import ArgumentParser
import pandas as pd
import sys

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('data_file', type=str, help='Input file')
    ap.add_argument('output_file', type=str, help='Output file')
    ap.add_argument('-sub', type=bool, help='Subsample classes', default=False)
    args = ap.parse_args()

    input_file = args.data_file
    output_file = args.output_file
    sub = args.sub
    in_file = pd.read_csv(input_file, sep='\t')
    new_data = []

    # rounding down for naturalness
    in_file.loc[in_file['naturalness'] == 1.5, 'naturalness'] = 1.0
    in_file.loc[in_file['naturalness'] == 2.5, 'naturalness'] = 2.0
    in_file.loc[in_file['naturalness'] == 3.5, 'naturalness'] = 3.0
    in_file.loc[in_file['naturalness'] == 4.5, 'naturalness'] = 4.0
    in_file.loc[in_file['naturalness'] == 5.5, 'naturalness'] = 5.0

    # rounding down for quality
    in_file.loc[in_file['quality'] == 1.5, 'quality'] = 1.0
    in_file.loc[in_file['quality'] == 2.5, 'quality'] = 2.0
    in_file.loc[in_file['quality'] == 3.5, 'quality'] = 3.0
    in_file.loc[in_file['quality'] == 4.5, 'quality'] = 4.0
    in_file.loc[in_file['quality'] == 5.5, 'quality'] = 5.0


    count_1 = 0
    count_2 = 0
    count_3 = 0
    count_4 = 0
    count_5 = 0
    count_6 = 0

    if sub:
        for key , value in in_file['naturalness'].iteritems():
            if value == 1.0 and count_1 < 1000:
                count_1 += 1
                new_data.append(in_file.iloc[key])
            elif value == 2.0 and count_2 < 1000:
                count_2 += 1
                new_data.append(in_file.iloc[key])
            elif value == 3.0 and count_3 < 1000:
                count_3 += 1
                new_data.append(in_file.iloc[key])
            elif value == 4.0 and count_4 < 1000:
                count_4 += 1
                new_data.append(in_file.iloc[key])
            elif value == 5.0 and count_5 < 1000:
                count_5 += 1
                new_data.append(in_file.iloc[key])
            elif value == 6.0 and count_6 < 1000:
                count_6 += 1
                new_data.append(in_file.iloc[key])
        # print(in_file.iloc[key])
    else:
        new_data = in_file

    # print(new_data)
    new_data = pd.DataFrame(new_data)
    new = new_data.to_csv(output_file, sep='\t', index=False)