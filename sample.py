# script that takes in input ouputs from neural systems (13 seq2seq + 3 data-driven) and randomly samples 10 sentences for each system (total of 160 sentences)
from argparse import ArgumentParser
import pandas as pd
import random
import sys
import os

ap = ArgumentParser()
ap.add_argument('-s', '--systems', nargs='+', help='list of files that contain outputs from neural NLG systems')
args = ap.parse_args()

new_df = pd.DataFrame(columns=['system', 'MR', 'output'])
for file in args.systems:
    base = os.path.basename(file)
    name_file = os.path.splitext(base)[0]
    temp_file = pd.read_csv(file, sep='\t')
    total_lines = len(temp_file.index)
    random_list = []
    for i in range(100):
        randn = random.randint(1, total_lines-1)  # excluding the header
        print('Random #: ', randn)
        new_df = new_df.append({'system': name_file, 'MR': temp_file.iloc[randn, 0], 'output': temp_file.iloc[randn, 1]}, ignore_index=True)

new_df = new_df.sample(frac=1).reset_index(drop=True)
final = new_df.to_csv(path_or_buf='samples_100.tsv', sep='\t', index=False)