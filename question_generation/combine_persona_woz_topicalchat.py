from argparse import ArgumentParser
import json
import csv
import sys
import re


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('first_file', help='first file Wizard of Wikipedia')
    ap.add_argument('second_file', help='second file Topical Chat')
    ap.add_argument('third_file', help='second file PersonaChat')
    args = ap.parse_args()

    first_file = args.first_file
    second_file = args.second_file
    third_file = args.third_file

    f_file = None
    s_file = None
    with open(first_file, 'r') as f_file:
        print('Loading Wizard of Wikipedia...')
        f_file = json.load(f_file)

    with open(second_file, 'r') as s_file:
        print('Loading Topical Chat...')
        s_file = json.load(s_file)

    final_post = []
    final_response = []
    # Wizard of Wikipedia
    previous_sentence = None
    print('Going through WoW...')
    for k in f_file:
        for utterance in k['dialog']:
            text = utterance['text']
            if isinstance(text, str):
                text = text.decode('utf-8')
            else:
                text = text.encode('utf-8')
            if '\n' in text:
                    text = text.replace('\n', ' ')
            if '?' in text and not previous_sentence is None:
                # add
                final_post.append(previous_sentence)
                final_response.append(text)

            previous_sentence = text


    print('Going through Topical Chat...')
    # Topical Chat
    previous_sentence = None
    for k,v in s_file.items():
        for message in v['content']:
            text = message['message']
            if isinstance(text, str):
                text = text.decode('utf-8')
            else:
                text = text.encode('utf-8')
            if '\n' in text:
                    text = text.replace('\n', ' ')
            if '?' in text and not previous_sentence is None:
                # add
                final_post.append(previous_sentence)
                final_response.append(text)

            previous_sentence = text


    print('Loading & Parsing PersonaChat...')
    previous_sentence = None
    """
        post: text
        response: labels
        end of dialogue: episode_done: True
    """
    with open(third_file, 'r') as t_file:
        for line in t_file:
            line = line.split('\t')
            for sentence in line:
                if isinstance(sentence, unicode):
                    sentence = sentence.encode('utf-8')
                else:
                    sentence = sentence.decode('utf-8')
                if not ':' in sentence:
                    continue
                sentence = sentence.split(':')[1]
                if '\n' in sentence:
                    sentence = sentence.replace('\n', ' ')
                if '?' in sentence and not previous_sentence is None:
                    final_post.append(previous_sentence)
                    final_response.append(sentence)
                previous_sentence = sentence


    print(len(final_post), len(final_response))
    # 2 different files for post & response
    with open('combination_files.post', 'w') as post_file:
        writer = csv.writer(post_file)
        count = 0
        for post in final_post:
            count += 1
            if count % 100 == 0:
                print('Writing {}/{} posts.'.format(count, len(final_post)))
            if isinstance(post, unicode):
                post = post.encode('utf-8')
            # else:
            #     post = post.decode('utf-8')
            writer.writerow([post])

    with open('combination_files.response', 'w') as response_file:
        writer = csv.writer(response_file)
        count = 0
        for response in final_response:
            count += 1
            if count % 100 == 0:
                print('Writing {}/{} responses.'.format(count, len(final_response)))
            if isinstance(response, unicode):
                response = response.encode('utf-8')
            writer.writerow([response])


