# taking in input the persona traits
from argparse import ArgumentParser
from models import InferSent
import numpy as np
import requests
import torch
import nltk
import time
#nltk.download('punkt')

class EntityLinker():
    def __init__(self):#, conf):
        super(EntityLinker, self)#.__init__(conf)

    def _get_entity_mentions(self, utterance, context=None, ignore_names=[]):

        try:
            annotations = requests.post(
                #self._params["linker_endpoint"],
                "http://localhost:8080",
                json={
                    "text": utterance.replace(".", "").replace("-", ""),
                    "properties": {},
                    "profanity": {
                        "http://www.wikidata.org/prop/direct/P31": [
                            "http://www.wikidata.org/entity/Q184439",
                            "http://www.wikidata.org/entity/Q608"
                        ],
                        "http://www.wikidata.org/prop/direct/P106": [
                            "http://www.wikidata.org/entity/Q488111",
                            "http://www.wikidata.org/entity/Q1027930",
                            "http://www.wikidata.org/entity/Q1141526"
                        ]
                    },
                    "annotationScore": -8.5,
                    "candidateScore": -10
                },
                #timeout=self._params.get("timeout", None)
            )

            if annotations.status_code != 200:
                print(
                    "[Entity Linker]: Status code %d\n"
                    "Something wrong happened. Check the status of the entity linking server and its endpoint." %
                    annotations.status_code)
                annotations = {}
            else:
                annotations = annotations.json()
        except requests.exceptions.RequestException as e:
            print("[Entity Linker]:"
                         "Something wrong happened. Check the status of the entity linking server and its endpoint.")
            annotations = {}
        return annotations

    def __call__(self, *args, **kwargs):

        annotations = kwargs.get("annotations", None)
        if not annotations:
            return None

        p_annotation = annotations.get("processed_text")

        # entity linking for the current user utterance (ignore when the user says their name)
        context = DictQuery(kwargs.get("context", {}))
        user_ents = None
        if p_annotation and not self._name_intent.search(p_annotation):
            user_ents = self._get_entity_mentions(p_annotation, context=context)
        return {
            "entity_linking": user_ents,
            #"bot_entities": bot_ents
        }


if __name__ == '__main__':

    ap = ArgumentParser()
    ent_link = EntityLinker()
    ap.add_argument('input_file', help='PersonaChat file as input')
    args = ap.parse_args()
    input_file = args.input_file
    sentences = []
    test_sentence = []
    with open(input_file, 'r') as in_file:
        print('Loading PersonaChat...')
        for line in in_file:
            if 'persona' in line:
                try:
                    line = line.split(':')[1]
                    sentences.append(line)
                except:
                    pass
            else:
                line = line.split('\t')[0]
                test_sentence.append(line)

    V = 2
    MODEL_PATH = 'encoder/infersent%s.pkl' % V
    params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': V}
    infersent = InferSent(params_model)
    print('Loading the model...')
    infersent.load_state_dict(torch.load(MODEL_PATH))

    W2V_PATH = 'fastText/crawl-300d-2M.vec'
    infersent.set_w2v_path(W2V_PATH)
    print('Building the vocabulary...')
    infersent.build_vocab(sentences, tokenize=True)
    print('Encoding the sentences...')
    embeddings = infersent.encode(sentences, tokenize=True)

    for sentence in test_sentence:
        # entity linker 
        entities = ent_link._get_entity_mentions(sentence)
        argmaxs, out, idxs = infersent.visualize(sentence, tokenize=True)
        sentence = sentence.split()
        try:
            t = int(sentence[0])
            del entities[sentence[0]]
            sentence = sentence[1:]
            argmaxs = argmaxs[1:]
        except:
            pass
        print(sentence)
        # find index of word in argmaxs 
        if len(sentence) == len(argmaxs):
            indexes = []
            # deal with entities of more than one word
            ents = []
            for ent, val in entities.items():
                ents.append(ent)
                ent = ent.split()
                tmp_index = []
                for each_ent in ent:
                    tmp_index.append(sentence.index(each_ent))
                indexes.append(tmp_index)
            important_argmaxs = []
            print(len(sentence),len(argmaxs))
            for index in indexes:
                print(index)
                tmp = [argmaxs[arg] for arg in index]
                important_argmaxs.append(np.sum(tmp)/len(tmp))
            
            #print([word for word in sentence.split(' ')])
            imp_perc = np.array([100.0 * n / np.sum(important_argmaxs) for n in important_argmaxs])

            # imp_perc = [imp_perc[sentence.index(ent)] for ent, val in entities.items()]
            # print(imp_perc)
            # find most important entity to talk about (!!!)
            print(ents[np.argmax(imp_perc)])
            print(np.max(imp_perc))
            time.sleep(1)