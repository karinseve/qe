from apted import APTED, Config
from argparse import ArgumentParser
from apted.helpers import Tree

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-ft', '--orig_tree', help='original reference tree')
    ap.add_argument('-st', '--sys_tree', help='system reference tree')
    args = ap.parse_args()

    tree1 = Tree.from_text(args.orig_tree)
    tree2 = Tree.from_text(args.sys_tree)
    apted = APTED(tree1, tree2)
    ted = apted.compute_edit_distance()
    mapping = apted.compute_edit_mapping()
    print(ted)
    for node1, node2 in mapping:
        print(node1, "->", node2)