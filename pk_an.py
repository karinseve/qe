from argparse import ArgumentParser
import pandas as pd

if __name__ == '__main__':
    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-i', '--input_file', help='file containing Topical-Chat Personale Knowledge sentences', required=True)
    args = ap.parse_args()

    input_file = args.input_file
    comm_sen = 0
    topic = 0
    pers_inf = 0
    opencl = 0
    lost = 0
    total = 0
    save = pd.DataFrame(columns=['utterance', 'annotation'])
    with open(input_file, 'r') as in_f:
        for line in in_f:
            print('\nUTTERANCE: {}\nANNOTATE:\t1. Common sense\t2. Topic-driven\t3. Personal info\t4. Open-close'.format(line))
            annot = input('Annotation:\t')
            save = save.append({'utterance': line, 'annotation': annot}, ignore_index=True)
            total += 1
            try:
                annot = int(annot)
                if annot == 1:
                    comm_sen += 1
                elif annot == 2:
                    topic += 1
                elif annot == 3:
                    pers_inf += 1
                elif annot == 4:
                    opencl += 1
            except ValueError:
                if annot == 'quit':
                    break
                else:
                    lost += 1

    end = save.to_csv('anno_output.csv', index=False)
    print('Total annotated utterances: {}\n\tCommon sense: {}\tTopic-driven: {}\tPersonal info: {}\tOpen-close: {}'.format(total, comm_sen, topic, pers_inf, opencl, lost))