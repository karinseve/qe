from SPARQLWrapper import SPARQLWrapper, JSON
from argparse import ArgumentParser
from bert_score import score
from random import choice, randint
from tqdm import tqdm
import networkx as nx
import numpy as np
import operator
import requests
import inflect
import scipy
import spacy
import json
import time
import sys

np.random.seed(42)
writing_json = {}
cache_el = {}
cache_bert = {}
possible_relations = ['RelatedTo', 'FormOf', 'IsA', 'PartOf', 'HasA', 'UsedFor', 'CapableOf', 'AtLocation', 'Causes', \
    'HasSubevent', 'HasFirstSubevent', 'HasLastSubevent', 'HasPrerequisite', 'HasProperty', 'MotivatedByGoal', \
    'ObstructedBy', 'Desires', 'CreatedBy', 'DistinctFrom', 'DerivedFrom', 'SymbolOf', 'DefinedAs', 'MannerOf', \
    'LocatedNear', 'HasContext', 'SimilarTo', 'CausesDesire', 'MadeOf', 'ReceivesAction', 'Other']


class EntityLinker():
    def __init__(self):#, conf):
        super(EntityLinker, self)

    def _get_entity_mentions(self, utterance, context=None, ignore_names=[]):

        try:
            annotations = requests.post(
                #self._params["linker_endpoint"],
                "http://localhost:8080",
                json={
                    "text": utterance.replace(".", "").replace("-", ""),
                    "properties": {},
                    "profanity": {
                        "http://www.wikidata.org/prop/direct/P31": [
                            "http://www.wikidata.org/entity/Q184439",
                            "http://www.wikidata.org/entity/Q608"
                        ],
                        "http://www.wikidata.org/prop/direct/P106": [
                            "http://www.wikidata.org/entity/Q488111",
                            "http://www.wikidata.org/entity/Q1027930",
                            "http://www.wikidata.org/entity/Q1141526"
                        ]
                    },
                    "annotationScore": -8.5,
                    "candidateScore": -10
                },
                #timeout=self._params.get("timeout", None)
            )

            if annotations.status_code != 200:
                print(
                    "[Entity Linker]: Status code %d\n"
                    "Something wrong happened. Check the status of the entity linking server and its endpoint." %
                    annotations.status_code)
                annotations = {}
            else:
                annotations = annotations.json()
        except requests.exceptions.RequestException as e:
            print("[Entity Linker]:"
                         "Something wrong happened. Check the status of the entity linking server and its endpoint.")
            annotations = {}
        return annotations


    def __call__(self, *args, **kwargs):

        annotations = kwargs.get("annotations", None)
        if not annotations:
            return None

        p_annotation = annotations.get("processed_text")

        # entity linking for the current user utterance (ignore when the user says their name)
        context = DictQuery(kwargs.get("context", {}))
        user_ents = None
        if p_annotation and not self._name_intent.search(p_annotation):
            user_ents = self._get_entity_mentions(p_annotation, context=context)
        return {
            "entity_linking": user_ents,
        }


FIND_IMPORTANT_NODES = """
    SELECT ?item ?itemLabel ?outcoming ?sitelinks ?incoming {
        wd:%s wdt:P279 ?item .
        ?item wikibase:statements ?outcoming .
        ?item wikibase:sitelinks ?sitelinks .
           {
           SELECT (count(?s) AS ?incoming) ?item WHERE {
               wd:%s wdt:P279 ?item .
               ?s ?p ?item .
               [] wikibase:directClaim ?p
          } GROUP BY ?item
      }
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en" . }.
    } ORDER BY DESC (?incoming)
"""


def find_id(url):
    entity = url.split('/')
    entity = entity[len(entity)-1]
    return entity


def find_entities(value):
    scores = []
    ids = []
    for val in value:
        id_ = val['entityLink']['identifier']
        score = val['score']
        ids.append(id_)
        scores.append(score)
    #scores = np.array(scores)
    min_val = min(ids, key=len)
    main_entity = find_id(min_val)
    return main_entity # returns Q item from Wikidata


def mid_way_step(G, names, ids, k, path, path_id):
    entities = [names[0]]
    ents = {}
    for x, z in zip(path, path_id):
        ents[x] = z
    entities_id = [ids[0]]
    index_ = 1
    start = names[0]
    while k > 0:
        tmp_scores = {}
        for ent_ind in range(index_, len(path)-k):
            current_entity = path[ent_ind]
            if (start, current_entity) in cache_bert:
                F1 = cache_bert[(start, current_entity)]
            else:
                P, R, F1 = score([start], [current_entity], lang='en', rescale_with_baseline=True)
                cache_bert[(start, current_entity)] = F1.item()
            tmp_scores[current_entity] = F1
        entity = max(tmp_scores, key=tmp_scores.get)
        entities.append(entity)
        entities_id.append(ents[entity])
        k -= 1
        start = entity
        index_ = path.index(entity)+1
    entities.append(names[1])
    entities_id.append(ids[1])
    return entities, entities_id


# check all sentences that contain the 2 chosen entities
def check_in_pchat(persona_file, rand_1_n, rand_2_n):
    p_one = []
    p_two = []
    with open(persona_file, 'r') as p_file:
        for line in p_file:
            if 'persona' in line:
                try:
                    line = line.split(':')[1]
                    if rand_1_n in line:
                        p_one.append(line)
                    elif rand_2_n in line:
                        p_two.append(line)
                except:
                    pass
    return p_one, p_two


def find_k(path):
    if len(path) <= 5:
        return 1
    elif len(path) <= 10:
        return 2
    else:
        return 3


# find if there's a path between 2 random nodes
def find_paths(G, persona_file, ent_1, value_1, rand_2, value_2):
    # entity names
    rand_1_n = ent_1
    rand_2_n = ent_2
    # entity IDs
    rand_1 = value_1[0]
    rand_2 = value_2[0]

    possible_short_paths = []
    other_paths = []
    try:
        t0 = time.clock()
        tmp_short = nx.all_shortest_paths(G, source=rand_1, target=rand_2)
        t1 = time.clock() - t0
        count = 0
        for path in tmp_short:
            if count >= 20:
                break
            possible_short_paths.append(path)
            count += 1
    except:
        return False, [], [], []
    if len(possible_short_paths) > 0:
        # choose random path amongst the found ones
        short_rand_var = np.random.randint(0, len(possible_short_paths))
        try:
            short_rand = [G.nodes[p]['name'] for p in possible_short_paths[short_rand_var]]
            path = [G.nodes[possible_short_paths[short_rand_var][0]]['name']]
            for node in range(len(possible_short_paths[short_rand_var])-1):
                path.append(G[possible_short_paths[short_rand_var][node]][possible_short_paths[short_rand_var][node+1]]['name'])
                path.append(G.nodes[possible_short_paths[short_rand_var][node+1]]['name'])
        except:
            # print(possible_short_paths[short_rand_var])
            return False, [], [], []
        mid_entities = []
        mid_entities_id = []
        # if path is shorter than 4 we don't need to crowdsource
        if len(short_rand) > 4:
            # print(len(short_rand))
            k = find_k(short_rand)
            mid_entities, mid_entities_id = mid_way_step(G, (rand_1_n, rand_2_n), (rand_1, rand_2), k, short_rand, possible_short_paths[short_rand_var])
            return True, short_rand, mid_entities, mid_entities_id
    return False, [], [], []


def scan_docs(ent_1, ent_2, articles, check_ent_1, k=2):
    mentions_one = []
    mentions_two = []
    for sentence in tqdm(articles):
        if check_ent_1 and ent_1 in sentence and len(mentions_one) < k:
            mentions_one.append(sentence)
        if ent_2 in sentence and len(mentions_two) < k:
            mentions_two.append(sentence)
    return mentions_one, mentions_two


def find_documents(entities, articles):
    entity_1 = entities[0]
    count = 0
    mentions_one = []
    mentions_two = []
    for entity in entities:
        if entity == entity_1:
            continue

        entity_2 = entity
        # TODO: entity_1 should be searching for mentions if unseen before
        if count == 0:
            temp_one, temp_two = scan_docs(entity_1, entity_2, articles, True)
        else:
            temp_one, temp_two = scan_docs(entity_1, entity_2, articles, False)
        if len(temp_one) > 0:
            mentions_one.append(temp_one)
            mentions_two.append(temp_two)
        # print(mentions_one)
        # print(mentions_two)
        # TODO: find importance score between mentions!
        mentions_one = mentions_two
        entity_1 = entity
        count += 1
    return mentions_one, mentions_two


def add_edges(G):
    for node in tqdm(list(G.nodes)):
        neighbours = G.successors(node)
        for neighbour in neighbours:
            G.add_edges_from([(neighbour, node, {'name': 'has_subclass'})])
        nx.write_gpickle(G, "pickled_graph_complete")


# NOT NEEDED ANYMORE
def remove_useless_links(G):
    # fnid meaningful connections
    sparql_client = SPARQLWrapper("http://query.wikidata.org/sparql", returnFormat=JSON, agent='User-Agent: Mozilla/5.0')
    sparql_client.setTimeout(604800)
    for node in tqdm(list(G.nodes)):
        # deleting root node and all its connections
        if node == 'root':
            G.remove_node(node)

        # find the important nodes, remove connections from current node to every other non-important
        sparql_client.setQuery(FIND_IMPORTANT_NODES % (node, node))
        results = sparql_client.queryAndConvert()['results']['bindings']
        time.sleep(3)
        if len(results) > 0:
            # check for node's neighbours
            neighbours = G.successors(node)
            remove = []
            for neighbour in neighbours:
                if neighbour not in results:
                    remove.append(neighbour)
            for rem in remove:
                G.remove_edge(node, rem)
        nx.write_gpickle(G, "pickled_graph_complete")


def remove_tokens(pos_sentence_one, pos_sentence_two, rand_1_entities, rand_2_entities):
    to_be_removed = ['AUX', 'PRON', 'SCONJ', 'DET', 'ADV', 'PART', 'ADP']
    to_be_removed_verbs = ['like', 'love', 'hate', 'enjoy']
    final_ent_1 = {}
    final_ent_2 = {}
    for token in pos_sentence_one:
        for ent, val in rand_1_entities.items():
            if token.text == ent and (token.pos_ in to_be_removed or token.text in to_be_removed_verbs or token.tag_ == 'MD'):
                continue
            if token.text == ent:
                final_ent_1[token.lemma_] = val
    # for ent in tmp_rem:
    #     if ent in rantmp_remd_1_entities:
    #         rand_1_entities.pop(ent)

    for token in pos_sentence_two:
        for ent, val in rand_2_entities.items():
            if token.text == ent and (token.pos_ in to_be_removed or token.text in to_be_removed_verbs or token.tag_ == 'MD'):
                continue
            if token.text == ent:
                final_ent_2[token.lemma_] = val
    # for ent in tmp_rem:
    #     if ent in rand_2_entities:
    #         rand_2_entities.pop(ent)
    return final_ent_1, final_ent_2


def find_max(ent_dict):
    rand = {}
    possible_max = []
    possible_max_ent = []
    max_ = -10
    for k, v in ent_dict.items():
        possible_max_ent.append(k)
        possible_max.append(v[1])
    possible_max = np.array(possible_max)
    arg_ = np.argmax(possible_max)
    rand[possible_max_ent[arg_]] = ent_dict[possible_max_ent[arg_]]
    return ent_dict, rand


# two entities in each trait, see how paths are
def find_max_entities(final_ent_1, final_ent_2, num_entities=2):
    rand_1 = None
    rand_2 = None
    while num_entities > 0 and bool(final_ent_1) and bool(final_ent_2):
        final_ent_1, rand_1 = find_max(final_ent_1)
        final_ent_2, rand_2 = find_max(final_ent_2)
        num_entities -= 1
    return rand_1, rand_2


def eval_conKB(entities):
    for i in range(len(entities)-1):
        entity_1 = entities[i]
        entity_2 = entities[i+1]

        out_conv = self.convKB(conv_input)
    return


# in_edges = boolean value (True finds incoming edges, False finds outgoing)
def find_edges(G, entity, value, in_edges, write):
    neighbours = []
    if in_edges:
        neighbours = G.in_edges([value_1[0]])
    else:
        neighbours = G.out_edges([value_1[0]])
    if write:
        writing_json[entity] = []
    for tuple_ in neighbours:
        try:
            name_1 = G.nodes[tuple_[0]]['name']
            name_2 = G.nodes[tuple_[1]]['name']
            if write:
                writing_json[entity].append([name_1, name_2])
        except:
            continue
    return len(neighbours)


def analyse_nodes(G, entity, value, write):
    # 1. look for num of edges coming in and out nodes
    # print(entity)
    outgoing = find_edges(G, entity, value, False, write)
    incoming = find_edges(G, entity, value, True, write)
    # print('Found {} outgoing and {} incoming edges from the node \'{}\''.format(outgoing, incoming, entity))
    return incoming, outgoing


def find_link(G, node_1, node_2):
    one_to_two = G.has_edge(node_1, node_2)
    two_to_one = G.has_edge(node_2, node_1)
    if one_to_two:
        return True, False
    elif two_to_one:
        return False, True


# if there is a path, no editing of the node names, only the links
def add_to_json_scenario1(G, path, id_, count):
    print(path)
    print(id_)
    save_path = {"nodes": [], "links": [], "dropdowns": []}
    for index in range(len(path)):
        node_to_be_added = {"id": index, "name": path[index], "group": 0 if index % (len(path)-1) == 0 else 1}
        save_path["nodes"].append(node_to_be_added)
        if index < len(path)-1:
            links_to_be_added = {"source": path[index], "target": path[index+1], "id": index}
            save_path["links"].append(links_to_be_added)

    for drop_index in range(len(save_path["links"])):
        # add here the incoming and outgoing nodes
        drop = {"type": "edge", "id": drop_index, "values": possible_relations}
        save_path["dropdowns"].append(drop)
    with open('paths_scenario1/edges_{}.json'.format(count), 'w') as out_f:
        json.dump(save_path, out_f, indent=4)


# entity is a tuple (name, wikidataID)
def get_neighbors(G, entity, start, end, save_path):
    if start == end:
        return save_path
    neighbor = G[entity[1]]
    temp_arr = []
    for ent, neighs in neighbor.items():
        # adding neighbor to dictionary
        tmp = {G.nodes[ent]["name"]: ent}
        temp_arr.append(tmp)
    temp_arr.append({'other': 'unknown_id'})
    save_path["dropdowns"].append({"type": "node", "id": str(start+1)+'_'+entity[0], "values": temp_arr})
    for ent, neighs in neighbor.items():
        if len(G[ent]) > 0:
            save_path = get_neighbors(G, (G.nodes[ent]["name"], ent), start+1, end, save_path)
    return save_path


# entitie + entities_id are 2 tuples - number of in-between entities is chosen randomly (3 entities tops)
def add_to_json_scenario2(G, entities, entities_id, count):
    save_path = {"nodes": [], "links": [], "dropdowns": []}
    p = inflect.engine()
    # adding starting entity
    count_entities = 0
    node_to_be_added = {"id": count_entities, "name": entities[0], "group": 0}
    count_entities += 1
    save_path["nodes"].append(node_to_be_added)
    # adding mid-entities (random number between 1 and 3)
    num_in_between = np.random.randint(3)
    for i in range(num_in_between):
        node_to_be_added = {"id": count_entities, "name": "entity_"+str(i), "group": 1}
        count_entities += 1
        save_path["nodes"].append(node_to_be_added)
    # adding final entities
    node_to_be_added = {"id": count_entities, "name": entities[1], "group": 0}
    save_path["nodes"].append(node_to_be_added)

    for index in range(len(save_path["nodes"])-1):
        links_to_be_added = {"source": save_path["nodes"][index], "target": save_path["nodes"][index+1], "id": index}
        save_path["links"].append(links_to_be_added)

    neighbours_final = {}
    # first node
    saved_node = entities[0]
    saved_node_id = entities_id[0][0]
    # generating dropdowns for nodes
    save_path = get_neighbors(G, (saved_node, saved_node_id), 0, len(save_path["nodes"])-2, save_path)
    # generating dropdowns for links
    for drop_index in range(len(save_path["links"])):
        # add here the incoming and outgoing nodes
        drop = {"type": "edge", "id": drop_index, "values": possible_relations}
        save_path["dropdowns"].append(drop)
    print(save_path)
    with open('paths_scenario2/edges_{}.json'.format(count), 'w') as out_f:
        json.dump(save_path, out_f, indent=4)
    sys.exit()


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('persona_file', help='PersonaChat file as input')
    # ap.add_argument('article_file', help='Article file as input')
    args = ap.parse_args()
    persona_file = args.persona_file
    # article_file = args.article_file

    with open(args.persona_file, 'r') as p_f:
        persona_traits = p_f.readlines()
    # persona_traits = []
    # with open(persona_file, 'r') as pers_file:
    #     for line in pers_file:
    #         if 'persona' in line:
    #             try:
    #                 line = line.split(':')
    #                 persona_traits.append(line[1])
    #             except:
    #                 pass

    # with open(article_file, 'r') as ar_file:
    #     articles = ar_file.readlines()

    G = nx.read_gpickle("kb_completion/enlarged_graph")
    print('Nodes: {}\tEdges: {}'.format(len(list(G.nodes)), len(list(G.edges))))
    spacy.prefer_gpu()
    nlp = spacy.load("en_core_web_sm")

    count_found = 0
    count_not_found = 0
    count_general = 0

    incoming_edges = []
    outgoing_edges = []
    write = False
    mid_entities = []
    try:
        for rand_1_num in tqdm(range(len(persona_traits)-1)):
            # choosing random persona traits from the same persona/next one
            # rand_1_num = randint(0, len(persona_traits)-1)
            rand_2_num = rand_1_num + 1

            # find entities in the traits
            linker = EntityLinker()
            if persona_traits[rand_1_num] in cache_el:
                rand_1_entities = cache_el[persona_traits[rand_1_num]]
            else:
                rand_1_entities = linker._get_entity_mentions(persona_traits[rand_1_num])
                cache_el[persona_traits[rand_1_num]] = rand_1_entities

            if persona_traits[rand_2_num] in cache_el:
                rand_2_entities = cache_el[persona_traits[rand_2_num]]
            else:
                rand_2_entities = linker._get_entity_mentions(persona_traits[rand_2_num])
                cache_el[persona_traits[rand_2_num]] = rand_2_entities

            pos_sentence_one = nlp(persona_traits[rand_1_num])
            pos_sentence_two = nlp(persona_traits[rand_2_num])

            # choose entity
            final_ent_1 = {}
            for ent, val in rand_1_entities.items():
                ident = val[0]['entityLink']['identifier'].split('/')[-1]
                final_ent_1[ent] = [ident, val[0]['score']]
                # print(ent)
            final_ent_2 = {}
            # print(persona_traits[rand_2_num])
            for ent, val in rand_2_entities.items():
                ident = val[0]['entityLink']['identifier'].split('/')[-1]
                final_ent_2[ent] = [ident, val[0]['score']]
                # print(ent)
            # removing verbs, pronouns from our entities
            rand_1_entities, rand_2_entities = remove_tokens(pos_sentence_one, pos_sentence_two, final_ent_1, final_ent_2)
            # print(rand_1_entities)
            # print(rand_2_entities)
            empty_one = not rand_1_entities
            empty_two = not rand_2_entities
            if empty_one or empty_two:
                count_general += 1
                continue
            # rand_1, rand_2 = find_max_entities(rand_1_entities, rand_2_entities, 2)
            # # if either one is empty, skip it
            # if not rand_1 or not rand_2:
            #     continue
            paths_storage = {(0, 0): (persona_traits[rand_1_num], persona_traits[rand_2_num])}
            for ent_1, value_1 in rand_1_entities.items():
                for ent_2, value_2 in rand_2_entities.items():
                    paths_storage[(ent_1, ent_2)] = []
                    if ent_1 == ent_2:
                        continue
                    found, shortest_path, entities, entities_id = find_paths(G, persona_file, ent_1, value_1, ent_2, value_2)
                    if found:
                        print(ent_1, ent_2)
                        paths_storage[(ent_1, ent_2)] = (shortest_path, entities)
                        mid_entities.append(len(entities))
                        add_to_json_scenario1(G, entities, entities_id, count_found)
                        count_found += 1
                        found = False
                    else:
                        add_to_json_scenario2(G, (ent_1, ent_2), (value_1, value_2), count_not_found)
                        count_not_found += 1
                        if count_general % 500 == 0:
                            write = True
                        incoming, outgoing = analyse_nodes(G, ent_1, value_1, write)
                        incoming_edges.append(incoming)
                        outgoing_edges.append(outgoing)
                        incoming, outgoing = analyse_nodes(G, ent_2, value_2, write)
                        incoming_edges.append(incoming)
                        outgoing_edges.append(outgoing)
                    count_general += 1
            # if printing:
            #     print(paths_storage)
    except KeyboardInterrupt:
        print('Exiting the loop')
    incoming_edges = np.array(incoming_edges)
    outgoing_edges = np.array(outgoing_edges)
    mid_entities = np.array(mid_entities)
    mode = scipy.stats.mode(mid_entities)

    print('{}/{} - {}'.format(count_found, count_general, count_found/count_general))
    print('Found {} MIN incoming edges, {} MAX incoming edges, {} AVERAGE incoming edges.'.format(np.amin(incoming_edges), np.amax(incoming_edges), np.mean(incoming_edges)))
    print('Found {} MIN outgoing edges, {} MAX outgoing edges, {} AVERAGE outgoing edges'.format(np.amin(outgoing_edges), np.amax(outgoing_edges), np.mean(outgoing_edges)))
    print('K mode: {}'.format(mode))

    #remove_useless_links(G)
    # add_edges(G)


