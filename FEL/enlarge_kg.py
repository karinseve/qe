from allennlp.predictors.predictor import Predictor
from read_pickle import EntityLinker
from argparse import ArgumentParser
import allennlp_models.syntax.srl
from tqdm import tqdm
import networkx as nx
import requests
import spacy
import time
import csv
import sys


def create_persona_traits(persona_file):
    persona_traits = []
    with open(persona_file, 'r') as pers_file:
        for line in pers_file:
            if 'persona' in line:
                try:
                    line = line.split(':')
                    persona_traits.append(line[1])
                except:
                    pass
    return persona_traits


# excluding all entities that are longer than 1 word and do not contain any noun/verb
def contains_noun(entity, pos_tag):
    entity = entity.split()
    if len(entity) > 1:
        pos = []
        for token in pos_tag:
            for ent in entity:
                if token.text == ent:
                    pos.append(token.pos_)
        if ('VERB' not in pos) or ('NOUN' not in pos):
            return False
    return True


# pos tagging of the sentence + entities recognised from the sentence
def remove_tokens(pos_sentence_one, rand_1_entities, ids):
    to_be_removed = ['AUX', 'PRON', 'SCONJ', 'DET', 'ADV', 'PART', 'ADP']
    to_be_removed_verbs = ['like', 'love', 'hate']
    tmp_rem = []
    remove = []
    for ent, val in rand_1_entities.items():
        if not contains_noun(ent, pos_sentence_one):
            remove.append(ent)
    for rem in remove:
        rand_1_entities.pop(rem)
    for token in pos_sentence_one:
        for ent, val in rand_1_entities.items():
            if token.pos_ in to_be_removed and token.text == ent:
                tmp_rem.append(ent)
            elif token.text in to_be_removed_verbs and token.text == ent:
                tmp_rem.append(ent)
    for ent in tmp_rem:
        if ent in rand_1_entities:
            rand_1_entities.pop(ent)
            ids.pop(ent)
    return rand_1_entities, ids


def find_max_entities(final_ent_1, num_entities=2):
    importance = []
    while num_entities > 0 and bool(final_ent_1):
        max_1 = max(final_ent_1, key=final_ent_1.get)
        importance.append(max_1)
        final_ent_1.pop(max_1)
        num_entities -= 1
    return importance


def find_entities(sentence, linker):
    entities = linker._get_entity_mentions(sentence)
    final_entities = {}
    final_ids = {}
    for ent, val in entities.items():
        wikidata_id = val[0]['entityLink']['identifier']
        tmp = wikidata_id.split('/')
        wikidata_id = tmp[-1]
        final_entities[ent] = val[0]['score']
        final_ids[ent] = wikidata_id
    return final_entities, final_ids


def find_in_doc(entities, articles):
    final_sentences = []
    for paragraph in articles:
        # split in sentences
        sentences = paragraph.split('.')
        sent = []
        found = [False for i in range(len(entities))]
        for sentence in sentences:
            for i in range(len(entities)):
                entity = entities[i]
                if entity in sentence:
                    found[i] = True
                    sent.append(sentence)
        if False in found:
            continue
        else:
            final_sentences.append(sent)
    return final_sentences


def enlarge_kg(sentences, linker):
    for sentence in sentences:
        sentence = sentence.split('\n')[0]
        # TODO: number of entities to find in sentence needs to be proportionate to the sentence length
        pos_tag = nlp(sentence)
        print(pos_tag)
        all_entities = find_entities(sentence, linker)
        print(all_entities)


def entity_in_sent(entities, tries):
    big_arg = []
    for entity in entities:
        for arg_ in tries:
            if entity in arg_:
                big_arg.append(tries)
    return big_arg

# def filter_sentences_using_docs(articles, entities, nlp):
    # predictor = Predictor.from_path("https://storage.googleapis.com/allennlp-public-models/bert-base-srl-2020.03.24.tar.gz")
    # if len(articles) == 0:
    #     return
    # for paragraph in articles:
    #     for sentence in paragraph:
    #         if '<abstract>' in sentence:  # first sentence of the abstract, probably what we want to keep
    #             sentence = sentence.split('>')[1]
    #             # TODO: replace spacy dependency parsing with allennlp semantic role labeling
    #             # sentence_tagging_ = nlp(sentence)
    #             # print('Spacy NLP tagging: ', sentence_tagging_)
    #             sentence_tagging = predictor.predict(sentence=sentence)
    #             res_sentence_tagging = sentence_tagging['verbs']
    #             for res in res_sentence_tagging:
    #                 role_label = res['description']
    #                 args = role_label.split('] ')

    #                 tries = []
    #                 for sent in args:
    #                     if ':' in sent:
    #                         tries.append(sent.split(': ')[1])
    #                         # print('Adding: ', tries)
    #                 if len(tries) == 3 and len(entity_in_sent(entities, tries)) > 0:
    #                     arguments.append(tries)


def remove_det(node, nlp):
    pos_node = nlp(node)
    final_node = node
    for token in pos_node:
        if token.pos_ == 'NOUN':
            final_node = token.text
    return final_node


def filter_sentences(articles, entities, nlp, enlarged_entities):
    filtered_sentences = {}
    # initialization of the dictionary
    for entity in entities:
        if entity in enlarged_entities:
            continue
        obj = requests.get('http://api.conceptnet.io/c/en/{}'.format(entity)).json()
        enlarged_entities.add(entity)
        final = []
        edges = obj['edges']
        try:
            tmp = obj['view']['nextPage']
            count = 0
            while tmp:
                count += 1
                if count > 5:
                    break
                tmp_ = requests.get('http://api.conceptnet.io{}'.format(tmp)).json()
                time.sleep(1)
                for edge in tmp_['edges']:
                    edges.append(edge)
                try:
                    tmp = tmp_['view']['nextPage']
                except:
                    break
        except:
            continue
        for edge in edges:
            if edge['rel']['label'] != 'Synonym':
                start_node = remove_det(edge['start']['label'], nlp)
                end_node = remove_det(edge['end']['label'], nlp)
                arguments = [start_node, edge['rel']['label'], end_node]
                final.append(arguments)
        filtered_sentences[entity] = final
        print('Found {} results for {}'.format(len(final), entity))
    return filtered_sentences, enlarged_entities


def get_entity_ID(entity):
    req = requests.get('https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&titles={}&format=json'.format(entity)).json()
    entity_id = req['query']['pages']
    return entity_id


def find_entity(graph, entity):
    graph_nodes = list(graph.nodes(data='name'))
    entity_id = [node[0] for node in graph_nodes if node[1] == entity]
    if len(entity_id) > 0:
        entity_id = entity_id[0]
    print('Entity: {}, ID: {}'.format(entity, entity_id))
    return [entity_id]


def add_nodes(graph, entity_one, entity_two, rel, id_one, id_two):
    graph.add_node(id_two, name=entity_two)
    graph.add_edges_from([(id_one, id_two, {'name': rel})])
    nx.write_gpickle(graph, 'enlarged_graph')


def adding_to_kg(graph, new_nodes, ids_import, linker, enlarged_entities):
    for entity in new_nodes.keys():
        entity_id = ids_import[entity]
        # print(entity, entity_id)
        for triple in new_nodes[entity]:
            # print(triple)
            if (entity in triple[0]) and (triple[2] != entity):
                # find wikidataID for other entity
                entity_two = triple[2].replace(' ', '_')
                if entity_two in enlarged_entities:
                    continue
                else:
                    tmp_ent, second_id = find_entities(triple[2], linker)
                    enlarged_entities.add(entity_two)
                    time.sleep(1.5)
                    try:
                        tmp_ent = tmp_ent[entity_two]
                        second_id = second_id[entity_two]
                        add_nodes(graph, entity, entity_two, triple[1], entity_id, second_id)
                    except:
                        # print('Not found: ', entity_two)
                        continue
            elif (entity in triple[2]) and (triple[0] != entity):
                entity_two = triple[0].replace(' ', '_')
                if entity_two in enlarged_entities:
                    continue
                else:
                    tmp_ent, second_id = find_entities(triple[0], linker)
                    enlarged_entities.add(entity_two)
                    time.sleep(1.5)
                    try:
                        tmp_ent = tmp_ent[entity_two]
                        second_id = second_id[entity_two]
                        add_nodes(graph, entity, entity_two, triple[1], entity_id, second_id)
                    except:
                        continue
    return enlarged_entities


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-k', '--knowledge_graph', help='Pickle of the KG')
    ap.add_argument('-p', '--persona_file', help='PersonaChat file')
    ap.add_argument('-d', '--document_file', help='Document file')
    ap.add_argument('-s', '--start_from', default='0', help='Starting point')
    spacy.prefer_gpu()
    nlp = spacy.load("en_core_web_sm")
    predictor = Predictor.from_path("https://storage.googleapis.com/allennlp-public-models/bert-base-srl-2020.03.24.tar.gz")
    args = ap.parse_args()
    with open(args.document_file, 'r') as in_f:
        articles = in_f.readlines()
    print('Creating Persona traits...')
    persona_traits = create_persona_traits(args.persona_file)
    starting_point = int(args.start_from)
    # with open(args.persona_file, 'r') as pers_file:
    #     persona_traits = pers_file.readlines()
    print('Reading graph...')
    knowledge_graph = nx.read_gpickle(args.knowledge_graph)
    linker = EntityLinker()
    final_write = []
    count = 0
    enlarged_entities = set()
    for trait in tqdm(persona_traits):
        count += 1
        if count < starting_point:
            continue
        trait = trait.rstrip('\n')
        print(trait)
        entities, ids = find_entities(trait, linker)
        print('POS tagging the trait...')
        pos_tag = nlp(trait)
        final_entities, final_ids = remove_tokens(pos_tag, entities, ids)
        ent_import = find_max_entities(final_entities, 2)
        lemmatized_ent = []
        new_ids = {}
        for ent in ent_import:
            new_ent = nlp(ent)
            for token in new_ent:
                lemmatized_ent.append(token.lemma_)
                new_ids[token.lemma_] = final_ids[ent]
        # doc_sentences = find_in_doc(lemmatized_ent, articles)
        # print(len(doc_sentences))
        doc_sentences = []
        filtered_sentences, enlarged_entities = filter_sentences(doc_sentences, lemmatized_ent, nlp, enlarged_entities)
        final_write.append([trait, lemmatized_ent, doc_sentences, len(doc_sentences)])
        if filtered_sentences:
            print('Adding to KG!')
            enlarged_entities = adding_to_kg(knowledge_graph, filtered_sentences, new_ids, linker, enlarged_entities)
