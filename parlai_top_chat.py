from argparse import ArgumentParser
import json
import sys

if __name__ == '__main__':
    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-t', '--topicalchat_file', help='file containing Topical Chat conversations', required=True)
    args = ap.parse_args()
    topical_chat = args.topicalchat_file

    conversations = {}
    with open(topical_chat, 'r') as t_chat:
        top_chat = json.load(t_chat)
        for key, dialog in top_chat.items():
            conversations[key] = []
            for conv in dialog['content']:
                conversations[key].append(conv['message'])
                    
    with open('test.txt', 'w') as wr:
        for k, v in conversations.items():
            count = 0
            # print(len(v))
            for utt in v:
                if count%2 == 0:
                    wr.write('text:{}\t'.format(utt))
                else:
                    wr.write('labels:{}\t'.format(utt))

                if count == (len(v)-1):
                    # print('End dialog')
                    wr.write('episode_done:True\t')
                count += 1