from argparse import ArgumentParser
import json
import sys
import re

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-i', '--input_file', help='file for question detection', required=True)
    args = ap.parse_args()

    input_file = args.input_file
    with open(input_file, 'r') as in_file:
    	in_file = json.load(in_file)
    	# Wizard of Wikipedia (2/166787 questions)
    	# dictionaries with topic as key
    	# value: array of senences

    	utt_count = 0
    	actual_match = 0
    	for k in in_file:
    		for utterance in k['dialog']:
    			utt_count += 1
    			text = utterance['text']
    			if '?' in text
    				actual_match += 1
    	
    	# Topical Chat
    	# dictionary (key: conversation)
    	# for k,v in in_file.items():
    	# 	for message in v['content']:
    	# 		utt_count += 1
    	# 		text = message['message']
    	# 		# print(text)
    	# 		# match_ = re.match(r'\?',  text)
    	# 		if '?' in text:
    	# 			# print(text)
    	# 			actual_match += 1
    	print('Match: {}/{}'.format(actual_match, utt_count))