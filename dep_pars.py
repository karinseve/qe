from allennlp.predictors.predictor import Predictor
from argparse import ArgumentParser
from apted import APTED, Config
from apted.helpers import Tree
import pandas as pd
import sys


# create dependency parsing tree (with words and POS tags)
def parse(in_tree, current_tree, pos):
    if isinstance(in_tree, list):
        for child in in_tree:
            current_tree += '{'
            if pos:
                current_tree += child['attributes'][0]
            else:
                current_tree += child['word']

            if 'children' in child:
                # current_tree, pos_tree = parse(child['children'], current_tree, pos_tree)
                current_tree = parse(child['children'], current_tree, pos) + '}'
            else:
                current_tree += '}'
    else:
        current_tree += '{'
        if pos:
            current_tree += in_tree['attributes'][0]
        else:
            current_tree += in_tree['word']

        if 'children' in in_tree:
            current_tree = parse(in_tree['children'], current_tree, pos)+ '}'
        else:
            current_tree += '}'

    return current_tree


# computes tree edit distance between tree1 and tree2
def ted(tree1, tree2):
    print(tree1)
    tree1 = Tree.from_text(tree1)
    tree2 = Tree.from_text(tree2)
    apted = APTED(tree1, tree2)
    ted = apted.compute_edit_distance()
    mapping = apted.compute_edit_mapping()
    print(ted)
    for node1, node2 in mapping:
        print(node1, "->", node2)
    print('-' * 15 + '\n')


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('input_file', type=str, help='file that contains all the sentences that need to be parsed')
    predictor = Predictor.from_path("https://s3-us-west-2.amazonaws.com/allennlp/models/biaffine-dependency-parser-ptb-2018.08.23.tar.gz")
    args = ap.parse_args()
    in_file = pd.read_csv(args.input_file, sep='\t')

    mr = None
    ref = None
    ref_dep = None
    pos_ref = None
    gold_sent = None
    _cmp = None
    _cmp_dep = None
    pos_cmp = None
    _cmp_sent = None
    for _sentence in in_file['sentence']:
        temp_mr = in_file.loc[in_file['sentence'] == _sentence]['mr'].values[0]

        # gold standard
        if mr != temp_mr:
            mr = temp_mr
            ref = predictor.predict(sentence=_sentence)
            ref_dep = parse(ref['hierplane_tree']['root'], '', False)
            pos_ref = parse(ref['hierplane_tree']['root'], '', True)
            gold_sent = _sentence
        else:
            _cmp = predictor.predict(sentence=_sentence)
            _cmp_dep = parse(_cmp['hierplane_tree']['root'], '', False)
            pos_cmp = parse(_cmp['hierplane_tree']['root'], '', True)
            _cmp_sent = _sentence

        if (ref is not None) and (_cmp is not None):
            print('Comparing...')
            # print('Gold Standard: ', gold_sent)
            # #print('POS Gold: ', pos_ref)
            # print('NLG System Output: ', _cmp_sent)
            # #print('POS comparison: ', pos_cmp)
            # print(ref)
            # print(_cmp)
            ted(ref_dep, _cmp_dep)
            ted(pos_ref, pos_cmp)