from argparse import ArgumentParser
import json

if __name__ == '__main__':
    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-i', '--input_file', help='file containing Topical Chat conversations', required=True)
    args = ap.parse_args()
    input_file = args.input_file

    with open(input_file, 'r') as i_file:
    	infile = json.load(i_file)
    	for line in infile:
    		if line['url'] == '/r/todayilearned':
    			print(line)