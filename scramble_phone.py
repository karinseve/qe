# this scripts scrambles all phone numbers

from argparse import ArgumentParser
import pandas as pd
import sys
import re

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('data_file', type=str, help='Input file')
    ap.add_argument('output_file', type=str, help='Output file')
    args = ap.parse_args()

    input_file = args.data_file
    output_file = args.output_file
    in_file = pd.read_csv(input_file, sep=',')

    in_file.replace(to_replace=r'(?=\d{10})\d{3}', value='555', regex=True, inplace=True)

    in_file.loc[:, 'informativeness'] = ''
    in_file.loc[:, 'quality'] = ''
    in_file.loc[:, 'naturalness'] = ''

    new = in_file.to_csv(output_file, sep='\t', index=False)
