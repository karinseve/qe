from argparse import ArgumentParser
import json

if __name__ == '__main__':
    ap = ArgumentParser()
    # more than one candidate file to compare
    ap.add_argument('-t', '--topicalchat_file', help='file containing Topical Chat conversations', required=True)
    args = ap.parse_args()
    topical_chat = args.topicalchat_file
    pk = []


    with open(topical_chat, 'r') as t_chat:
        num_mess = 0
        num_pk = 0
        only_pk = 0
        top_chat = json.load(t_chat)
        for k,v in top_chat.items():
            for message in v['content']:
                num_mess += 1
                knowledge_list = message['knowledge_source']
                if "Personal Knowledge" in knowledge_list:
                    pk.append(message['message'])
                    num_pk += 1
                    if len(knowledge_list) == 1:
                        only_pk += 1

        perc = num_pk*100/num_mess
        print('Personal Knowledge: {0}/{1} - {2:.2f}%'.format(num_pk, num_mess, perc))
        print('Only PK: {0}/{1} - {2:.2f}%'.format(only_pk, num_pk, only_pk*100/num_pk))

        print('Writing to file...')
        with open('personal_knowledge.txt', 'w') as wf:
            for sentence in pk:
                wf.write('{}\n'.format(sentence))