from argparse import ArgumentParser
import pandas as pd
import edlib


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('human_references', type=str, help='File with human references')
    ap.add_argument('data_file', type=str, help='To check')
    args = ap.parse_args()

    hum_ref = args.human_references
    in_file = pd.read_csv(hum_ref)
    to_compare = pd.read_csv(args.data_file)


    in_file['mr'].replace(to_replace=r'\[', value='=', regex=True, inplace=True)
    in_file['mr'].replace(to_replace=r'\]', value='', regex=True, inplace=True)
    in_file['mr'] = 'inform(' + in_file['mr'] + ')'
    print(in_file['mr'])

    for ex in in_file:
    	main_mr_len = len(ex['mr'])
    	for s in to_compare:
    		result = edlib.align(ex['mr'], s['mr'])
    		if result/main_mr_len < 0.2:
    			print(ex['mr'], s['mr'])