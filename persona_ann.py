"""
	Takes in input the PersonaChat txt file
	Annotate whether each sentence is coherent or not (bool value)
	Save previous + current sentence in csv file with coherence value

	If output file is already existing, reload it and scroll persona file until left-off point has been reached
"""
from argparse import ArgumentParser
import csv

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('annotation_file', help='first file PersonaChat')
    ap.add_argument('-o', '--output_file', help='output file')
    args = ap.parse_args()
    input_file = args.annotation_file
    output_file = args.output_file

    

