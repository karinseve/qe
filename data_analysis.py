# mean, median among 3 ratings of a sentence
from rpy2.robjects.packages import importr
from argparse import ArgumentParser
from fleiss import fleissKappa
import pandas as pd
import numpy as np
import statistics
import sys

def icc(x,y=None,verbose=0):
    """
Calculates intraclass correlation coefficients using simple, Type I sums of squares.
If only one variable is passed, assumed it's an Nx2 matrix
Usage:   icc(x,y=None,verbose=0)
Returns: icc rho, prob ####PROB IS A GUESS BASED ON PEARSON
"""
    TINY = 1.0e-20
    if y:
        all = N.concatenate([x,y],0)
    else:
        all = x+0
        x = all[:,0]
        y = all[:,1]
    totalss = ass(all-mean(all))
    pairmeans = (x+y)/2.
    withinss = ass(x-pairmeans) + ass(y-pairmeans)
    withindf = float(len(x))
    betwdf = float(len(x)-1)
    withinms = withinss / withindf
    betweenms = (totalss-withinss) / betwdf
    rho = (betweenms-withinms)/(withinms+betweenms)
    t = rho*math.sqrt(betwdf/((1.0-rho+TINY)*(1.0+rho+TINY)))
    prob = abetai(0.5*betwdf,0.5,betwdf/(betwdf+t*t),verbose)
    return rho, prob


ap = ArgumentParser()
ap.add_argument('input_file', type=str, help='file that contains all the sentences that need to be parsed')
ap.add_argument('old_file', type=str, help='file that contains all the sentences that need to be parsed')
args = ap.parse_args()

input_file = pd.read_csv(args.input_file, sep=',', header=0)
old_file = pd.read_csv(args.old_file, sep=',', header=0)

new_file = pd.DataFrame(columns=['system', 'MR', 'output', 'informativeness_avg', 'quality_avg', 'naturalness_avg', 'informativeness_med', 'quality_med', 'naturalness_med'])
dict_inf = {}
dict_nat = {}
dict_qual = {}
search = {}
count_key_inf = {}
count_key_nat = {}
count_key_qual = {}

changed_inf = 0
change_inf = []
not_changed_inf = 0
positive_change_inf = 0
negative_change_inf = 0

changed_nat = 0
change_nat = []
not_changed_nat = 0
positive_change_nat = 0
negative_change_nat = 0

changed_qual = 0
change_qual = []
not_changed_qual = 0
positive_change_qual = 0
negative_change_qual = 0
for line in input_file.index:
    # Fleiss kappa
    key = (input_file.iloc[line]['MR'], input_file.iloc[line]['output'])
    # search[key] =input_file['system'][line]

    # if key in dict_inf:
    #     if count_key_inf[key] < 3:
    #         #print('Line: {} Found key: {}'.format(line, key))
    #         dict_inf[key].append(input_file['informativeness'][line])
    #         count_key_inf[key] += 1
    # else:
    #     #print('Line: {} Adding key: {}'.format(line, key))
    #     dict_inf[key] = [input_file['informativeness'][line]]
    #     count_key_inf[key] = 1

    # if key in dict_nat:
    #     if count_key_nat[key] < 3:
    #         dict_nat[key].append(input_file['naturalness'][line])
    #         count_key_nat[key] += 1
    # else:
    #     dict_nat[key] = [input_file['naturalness'][line]]
    #     count_key_nat[key] = 1

    # if key in dict_qual:
    #     if count_key_qual[key] < 3:
    #         dict_qual[key].append(input_file['quality'][line])
    #         count_key_qual[key] += 1
    # else:
    #     dict_qual[key] = [input_file['quality'][line]]
    #     count_key_qual[key] = 1
    #if (old_file.loc[old_file['MR'] == key[0]].empty) and (old_file.loc[old_file['output'] == key[1]].empty):
    try:
        old_index = old_file.loc[(old_file['MR'] == key[0]) & (old_file['output'] == key[1])]
        if not old_index.empty:
            old_inf = old_index['informativeness_avg'].item()
            new_inf = input_file.iloc[line]['informativeness_avg'].item()
            old_nat = old_index['naturalness_avg'].item()
            new_nat = input_file.iloc[line]['naturalness_avg'].item()
            old_qual = old_index['quality_avg'].item()
            new_qual = input_file.iloc[line]['quality_avg'].item()

            if new_inf != old_inf:
                changed_inf += 1
                change_inf.append(abs(new_inf-old_inf))
                if new_inf > old_inf:
                    positive_change_inf +=1
                else:
                    negative_change_inf += 1
            else:
                not_changed_inf += 1

            if new_nat != old_nat:
                changed_nat += 1
                change_nat.append(abs(new_nat-old_nat))
                if new_nat > old_nat:
                    positive_change_nat += 1
                else:
                    negative_change_nat += 1
            else:
                not_changed_nat += 1

            if new_qual != old_qual:
                changed_qual += 1
                change_qual.append(abs(new_qual-old_qual))
                if new_qual > old_qual:
                    positive_change_qual += 1
                else:
                    negative_change_qual += 1
            else:
                not_changed_qual += 1
    except KeyError:
        print('Not found')
change_inf = np.array(change_inf)
change_nat = np.array(change_nat)
change_qual = np.array(change_qual)

print('INFORMATIVENESS - changed: {}, not changed: {}, positive changes: {}, negative change: {}, avg-change: {}, median change: {}, std: {}, var: {}'.format(changed_inf, not_changed_inf, positive_change_inf, negative_change_inf, np.mean(change_inf), np.median(change_inf), np.std(change_inf), np.var(change_inf)))
print('NATURALNESS - changed: {}, not changed: {}, positive changes: {}, negative change: {}, avg-change: {}, median change: {}, std: {}, var: {}'.format(changed_nat, not_changed_nat, positive_change_nat, negative_change_nat, np.mean(change_nat), np.median(change_nat), np.std(change_nat), np.var(change_nat)))
print('QUALITY - changed: {}, not changed: {}, positive changes: {}, negative change: {}, avg-change: {}, median change: {}, std: {}, var: {}'.format(changed_qual, not_changed_qual, positive_change_qual, negative_change_qual, np.mean(change_qual), np.median(change_qual), np.std(change_qual), np.var(change_qual)))

sys.exit()


total_inf = []
total_nat = []
total_qual = []
table_inf = np.zeros((len(dict_inf.items()), 6))
table_nat = np.zeros((len(dict_inf.items()), 6))
table_qual = np.zeros((len(dict_inf.items()), 6))
# minimizing classes to 2 instead of 6 (<=3, >3)
table_inf_2 = np.zeros((len(dict_inf.items()), 2))
table_nat_2 = np.zeros((len(dict_inf.items()), 2))
table_qual_2 = np.zeros((len(dict_inf.items()), 2))

icc = np.zeros(len(dict_inf.items()), 3)

ind = 0
for key, value in dict_inf.items():
    temp_inf = []
    temp_nat = []
    temp_qual = []
    for val in value:
        total_inf.append(val)
        temp_inf.append(val)
        table_inf[ind, val-1] += 1
        if val <= 3:
            table_inf_2[ind, 0] += 1
        else:
            table_inf_2[ind, 1] += 1
    for val in dict_nat[key]:
        total_nat.append(val)
        temp_nat.append(val)
        table_nat[ind, val-1] += 1
        if val <= 3:
            table_nat_2[ind, 0] += 1
        else:
            table_nat_2[ind, 1] += 1
    for val in dict_qual[key]:
        icc[key, val.index()] = val

        total_qual.append(val)
        temp_qual.append(val)
        table_qual[ind, val-1] += 1
        if val <= 3:
            table_qual_2[ind, 0] += 1
        else:
            table_qual_2[ind, 1] += 1

    new_file = new_file.append({'system': search[key], 'MR': key[0], 'output': key[1], 'informativeness_avg': statistics.mean(temp_inf), 'quality_avg': statistics.mean(temp_qual), 'naturalness_avg': statistics.mean(temp_nat), 'informativeness_med': statistics.median(temp_inf), 'quality_med': statistics.median(temp_qual), 'naturalness_med': statistics.median(temp_nat)}, ignore_index=True)
    ind += 1

raters = 3

# 6 total classes
table_inf = table_inf.astype(int).tolist()
print('6 classes informativeness fleiss k: {}\n'.format(fleissKappa(table_inf, 3)))

# 2 classes
table_inf_2 = table_inf_2.astype(int).tolist()
print('2 classes informativeness fleiss k: {}\n'.format(fleissKappa(table_inf_2, 3)))



table_nat = table_nat.astype(int).tolist()
print('6 classes naturalness fleiss k: {}\n'.format(fleissKappa(table_nat, 3)))

# 2 classes
table_nat_2 = table_nat_2.astype(int).tolist()
print('2 classes naturalness fleiss k: {}\n'.format(fleissKappa(table_nat_2, 3)))


table_qual = table_qual.astype(int).tolist()
print('6 classes quality fleiss k: {}\n'.format(fleissKappa(table_qual, 3)))

# 2 classes
table_qual_2 = table_qual_2.astype(int).tolist()
print('2 classes quality fleiss k: {}\n'.format(fleissKappa(table_qual_2, 3)))


print('INFORMATIVENESS Total mean: {}; total median: {};'.format(statistics.mean(total_inf), statistics.median(total_inf)))
print('NATURALNESS Total mean: {}; total median: {};'.format(statistics.mean(total_nat), statistics.median(total_nat)))
print('QUALITY Total mean: {}; total median: {};'.format(statistics.mean(total_qual), statistics.median(total_qual)))


# save = new_file.to_csv(path_or_buf='error_an_prev.csv', index=False)

res = psy.icc(icc)
print(res.agreement)
