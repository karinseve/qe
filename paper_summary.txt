Learing to ask questions in open-domain conversational systems with typed decoders

WHAT THE PAPER TRIES TO DO: This task, substantially different from traditional question  generation, requires to question not only with various patterns but also on diverse and relevant topics.

- question generation in open-domain conversational systems aims at enhancing the interactiveness and persistence of human-machine interactions

- issues with asking good questions: questioning  with  diversified  patterns, and addressing transitional topics naturally in a generated question

- classifying words in a question into 3 types: interrogative (who, what, where,..), topic word (addressing key information), ordinary word (syntactical & grammatical roles)

2 TYPES OF DECODERS:
	- Soft Typed Decoder (STD) - applies type-specific generation distributions | type probabilities are the coefficients
	- Hard Typed Decoder (HTD) - applies Gumbel-softmax to reshape the type distributions

PMI: Pointwise Mutual Information - measures association between a feature and a class [in the paper, PMI with words to understand whether words x and y occurr in post or response] --> used to predict a set of topic words for an input post 
	- used to decide whether a feature is informative or not

DATASET:
- 9 million post-response pairs from Weibo
- pairs are distilled: responses are in question form (20 hand-crafted templates detecting words like 'what', 'how many', ... or ending with '?').
- further distillation; removed questions with universal responses

- final dataset: 491k post-response pairs (5k val + 5k test)
- avg # words post: 8.3
- avg # words response: 9.3
- 66547 different words
- 18717 words appear more than 10 times


BASELINES:
- seq2seq with attention
- MA (mechanism-aware): randomly picks answer from generated answers
- TA (topic-aware): generates informative responses by incorporating topic words
- ERM (elastic responding machine): selects a subset of responding mechanisms using RL



AUTOMATIC EVALUATION METRICS:
- perplexity
- distinct-1 & distinct-2 to evaluate the diversity of the responses (calculate the proportion of the total number of distinct uni/bi-grams to the total number of generated tokens in all generated responses)
- TRR (topical response ratio): proportion of responses containing at least one topic word in the list predicted by PMI

HUMAN EVALUATION METRICS:
- appropriateness: question reasonable in logic and content, key information?
- richness: contains relevant topic words
- willingness to respond: whether a user will respond (does it elicit further interactions?)

ERROR ANALYSIS:
- no topic words in a response (NoT)
- wrong topics (WrT)
- type generation error (wrong word type is predicted) -> causes grammatical error (TGE)
- other errors



CONCLUSIONS:
HTD better than STD (better automatic evaluation [besides perplexity], better human evaluation, less NoT errors, more WrT errors)
