- find file that has specific number of lines (in between / /)

find . -name "train.tsv" -type f | xargs wc -l | awk '/163765/ {print}'


- checking class imbalance (counting number of occurrences of score 6.0 in column 19)

cut -f19 train.tsv | awk '/6.0/{++cnt} END {print cnt}'


- find files that contain specific word (searching through subdirectories)

grep -wRl "inform(name=none,area='city centre',near=X-near)" ./


- print line that contain specific word

grep -w "inform(name=none,area='city centre',near=X-near)" ./noref_Fall/cv00/train.tsv 


- print entire lines where column 12 is 1.0:

awk '$12 == "1.0" {print $0}' train.tsv 


- print lines where column 12 is 1.0 and column 16 matches mr:

awk -F "\t" '{if($12 == "1.0" && $16=="inform(type=\047hotel\047,count=\0472\047,near=\047cole valley\047)") print $0}' train.tsv 