#!/usr/bin/env python
# -"- coding: utf-8 -"-

"""
Build a confusion matrix and print it as CSV, given a TSV file with classification
"""

from __future__ import unicode_literals

import pandas as pd
import sklearn.metrics
import numpy as np
import math
from argparse import ArgumentParser


def add_labels(matrix, labels):
    return [[''] + labels] + [[label] + row for label, row in zip(labels, matrix.tolist())]


def process_file(data_file):

    df = pd.read_csv(data_file, index_col=None, sep='\t')
    human = [str(val) for val in df['quality_human_rating']]
    system = [str(round(val)) for val in df['quality_system_rating']]
    #system = [round(float(val)) for val in system]
    labels = sorted(list(set(human).union(set(system))))

    M = sklearn.metrics.confusion_matrix(human, system, labels=labels)
    M_perc = np.array([[0 if sum(row) == 0 else float(x) / sum(row) for x in row] for row in M])

    # parsing confusion matrix
    true_pos = np.diag(M_perc)
    false_pos = np.sum(M_perc, axis=0) - true_pos
    false_neg = np.sum(M_perc, axis=1) - true_pos
    # precision = np.sum(true_pos / (true_pos + false_pos))/len(true_pos)
    # if math.isnan(precision):
    #     precision = 0
    mini_prec = []
    for val, val_ in zip(true_pos, false_pos):
        if val != 0:
            mini_prec.append(val / (val + val_))
        else:
            mini_prec.append(0)
    fake_pre = np.array(mini_prec)
    precision = np.sum(fake_pre)/len(true_pos)
    recall = np.sum(true_pos / (true_pos + false_neg))/len(true_pos)
    f1 = 2*((precision*recall)/(precision+recall))
    num_classes = 11
    tn = []
    for i in range(num_classes):
        temp = np.delete(M_perc, i, 0)    # delete ith row
        temp = np.delete(temp, i, 1)  # delete ith column
        tn.append(sum(sum(temp)))

    spec = np.sum(tn/(tn+false_pos))/len(true_pos)
    bal_acc = (recall+spec)/2

    M = add_labels(M, labels)
    M_perc = add_labels(M_perc, labels)

    out = pd.DataFrame(M + [[]] + M_perc)
    print(out.to_csv(header=False,index=False))
    print('Precision: {}, recall: {}, F1: {}, bal_acc: {}'.format(precision, recall, f1, bal_acc))


if __name__ == '__main__':

    ap = ArgumentParser()
    ap.add_argument('classif_file', type=str, help='TSV file with classification')

    args = ap.parse_args()
    process_file(args.classif_file)
