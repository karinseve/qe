#!/bin/bash

# srun --partition=amd-longq -c 16 --output=file.txt


echo "Running exp 1/2"
./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v3.3cv/noref/cv00/train_sub.tsv model_sub.pickle.gz
./run_ratpred.py test -w out_sub.tsv model_sub.pickle.gz ../data/preproc_data/v3.3cv/noref/cv00/test_sub.tsv


echo "Running exp 2/2"
./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v3.3cv/noref_fs/cv00/train_sub.tsv model_s1_sub.pickle.gz
./run_ratpred.py test -w out_s1_sub.tsv model_s1_sub.pickle.gz ../data/preproc_data/v3.3cv/noref_fs/cv00/test_sub.tsv
