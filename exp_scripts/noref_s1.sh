#!/bin/bash


# srun --partition=amd-longq -c 16 --output=file.txt


# NEM dataset, no reference + S1 from Dusek 2017
for i in {0..4}; do
	echo "Running exp $i/4"
	if [[ $i -ne 0 ]]; then
		./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v3.3cv/noref_fs/cv0$i/train.tsv model_s1.pickle.gz
	fi
	./run_ratpred.py test -w out_$i.tsv model_s1.pickle.gz ../data/preproc_data/v3.3cv/noref_fs/cv0$i/test.tsv
done
