#!/bin/bash


# srun --partition=amd-longq -c 16 --output=file.txt


# NEM dataset, no reference, basic dataset
for i in {0..4}; do
	echo "Running exp $i/4"
	./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v3.3cv/noref_Ftonly/cv0$i/train.tsv model_s2.pickle.gz
	./run_ratpred.py test -w out_s2_$i.tsv model_s2.pickle.gz ../data/preproc_data/v3.3cv/noref_Ftonly/cv0$i/test.tsv
done


