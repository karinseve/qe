#!/bin/bash

echo "Running exp 4/4"
./run_ratpred.py train experiments/config/config_nohalves.yaml ../data/preproc_data/v3.3cv/noref_Fall/cv00/train_sub.tsv models/model_s4_sub_rr.pickle.gz
./run_ratpred.py test -w outputs/out_s4_sub_rr.tsv models/model_s4_sub_rr.pickle.gz ../data/preproc_data/v3.3cv/noref_Fall/cv00/devel_sub.tsv