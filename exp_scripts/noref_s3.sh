#!/bin/bash


# srun --partition=amd-longq -c 16 --output=file.txt


# NEM dataset, no reference, basic dataset
# for i in {0..4}; do
# 	echo "Running exp $i/4"
./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v3.3cv/noref_Fall/cv00/train.tsv models/model_s3.pickle.gz
./run_ratpred.py test -w outputs/out_s3_$i.tsv models/model_s3.pickle.gz ../data/preproc_data/v3.3cv/noref_Fall/cv00/dev.tsv
# done


