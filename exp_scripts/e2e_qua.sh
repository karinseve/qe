#!/bin/bash


# srun --partition=amd-longq -c 16 --output=file.txt


# NEM dataset, no reference, basic dataset
for i in {0..4}; do
	echo "Running exp $i/4"
	./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v4/e2e_qua/train.tsv model_e2e.pickle.gz
	./run_ratpred.py test -w out_e2e_$i.tsv model_e2e.pickle.gz ../data/preproc_data/v4/e2e_qua/test.tsv
done


