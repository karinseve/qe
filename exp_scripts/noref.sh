#!/bin/bash


# srun --partition=amd-longq -c 16 --output=file.txt


# NEM dataset, no reference, basic dataset
for i in {0..4}; do
	echo "Running exp $i/4"
	./run_ratpred.py train experiments/config/config.yaml ../data/preproc_data/v3.3cv/noref/cv0$i/train.tsv model.pickle.gz -i 1476
	./run_ratpred.py test -w out_$i.tsv model.pickle.gz ../data/preproc_data/v3.3cv/noref/cv0$i/dev.tsv
done


