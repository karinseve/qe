#!/bin/bash

# srun --partition=amd-longq --gres=gpu -c 16 --output=file.txt


echo "Running exp 2/4"
./run_ratpred.py train experiments/config/config_nohalves.yaml ../data/preproc_data/v3.3cv/noref_fs/cv00/train_sub_comb.tsv models/model_s2_sub_rr.pickle.gz
./run_ratpred.py test -w outputs/out_s2_sub_rr.tsv models/model_s2_sub_rr.pickle.gz ../data/preproc_data/v3.3cv/noref_fs/cv00/devel_sub.tsv
