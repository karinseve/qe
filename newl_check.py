"""
    checking for new lines in text files
"""
from argparse import ArgumentParser
import json

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('input_file')
    ap.add_argument('-w', type=bool, default=True)
    args = ap.parse_args()

    input_file = args.input_file

    in_file = None
    with open(input_file, 'r') as i_file:
        in_file = json.load(i_file)

    end_line = 0
    # Wizard of Wikipedia
    # for k in in_file:
    #     for utterance in k['dialog']:
    #         text = utterance['text'].encode('utf-8')
    #         if '\n' in text:
    #             end_line += 1


    # Topical Chat
    for k,v in in_file.items():
        for message in v['content']:
            text = message['message'].encode('utf-8')
            if '\n' in text:
                end_line += 1

    print('Found {} newline in sentences.'.format(end_line))