from argparse import ArgumentParser
import pandas as pd
import sys

ap = ArgumentParser()
ap.add_argument('input_file', type=str, help='file that contains all the sentences that need to be parsed')
args = ap.parse_args()

input_file = pd.read_csv(args.input_file, sep=',')
final_data = pd.DataFrame(columns=['system', 'mr', 'system_ref', 'informativeness', 'quality', 'naturalness'])
for line in input_file.index:
    inf = [input_file['Answer.inf.one.1'][line], input_file['Answer.inf.two.2'][line], input_file['Answer.inf.three.3'][line], input_file['Answer.inf.four.4'][line], input_file['Answer.inf.five.5'][line], input_file['Answer.inf.six.6'][line]]
    nat = [input_file['Answer.nat.one.1'][line], input_file['Answer.nat.two.2'][line], input_file['Answer.nat.three.3'][line], input_file['Answer.nat.four.4'][line], input_file['Answer.nat.five.5'][line], input_file['Answer.nat.six.6'][line]]
    qual = [input_file['Answer.qual.one.1'][line], input_file['Answer.qual.two.2'][line], input_file['Answer.qual.three.3'][line], input_file['Answer.qual.four.4'][line], input_file['Answer.qual.five.5'][line], input_file['Answer.qual.six.6'][line]]

    informativeness = [i for i, x in enumerate(inf) if x]
    naturalness = [i for i, x in enumerate(nat) if x]
    quality = [i for i, x in enumerate(qual) if x]

    # when no input, input becomes avg between the other 2
    if len(informativeness) == 0:
        try:
            naturalness = int(''.join(map(str, naturalness)))+1
            quality = int(''.join(map(str, quality)))+1
            informativeness = [(naturalness+quality)/2]
        except:
            print('Line {}'.format(line))
            row = final_data.loc[(final_data['mr'] == input_file['Input.mr'][line]) & (final_data['system_ref'] == input_file['Input.sys_ref'][line])]
            informativeness = row['informativeness']
            print('Made it: ', informativeness)
    elif len(naturalness) == 0:
        try:
            informativeness = int(''.join(map(str, informativeness)))+1
            quality = int(''.join(map(str, quality)))+1
            naturalness = [(informativeness+quality)/2]
        except:
            print('Line {}'.format(line))
    elif len(quality) == 0:
        try:
            informativeness = int(''.join(map(str, informativeness)))+1
            naturalness = int(''.join(map(str, naturalness)))+1
            quality = [(informativeness+naturalness)/2]
        except:
            print('Line {}'.format(line))
    else:
        informativeness = int(''.join(map(str, informativeness)))+1
        naturalness = int(''.join(map(str, naturalness)))+1
        quality = int(''.join(map(str, quality)))+1

    #print(informativeness)
    #sys.exit()

    final_data = final_data.append({'system': input_file['Input.system'][line], 'mr': input_file['Input.mr'][line], 'system_ref': input_file['Input.sys_ref'][line], 'informativeness': informativeness, 'quality': quality, 'naturalness': naturalness}, ignore_index=True)

save = final_data.to_csv(path_or_buf='indiv_ratings_parsed.csv', index=False)