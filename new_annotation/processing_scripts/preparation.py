from argparse import ArgumentParser
import pandas as pd
import os

ap = ArgumentParser()
ap.add_argument('input_file', type=str, help='file that contains all the sentences that need to be parsed')
ap.add_argument('-m', default=False, action='store_true')  # use the median instead of the mean
args = ap.parse_args()

filename = os.path.splitext(args.input_file)[0]
input_file = pd.read_csv(args.input_file, sep=',')
new_file = pd.DataFrame(columns=['system', 'mr', 'system_ref', 'orig_ref', 'informativeness', 'quality', 'naturalness'])

for line in input_file.index:
    informativeness = input_file['informativeness_avg'][line]
    naturalness = input_file['naturalness_avg'][line]
    quality = input_file['quality_avg'][line]
    if args.m:
        informativeness = input_file['informativeness_med'][line]
        naturalness = input_file['naturalness_med'][line]
        quality = input_file['quality_med'][line]
    new_file = new_file.append({'system': input_file['system'][line], 'mr': input_file['MR'][line], 'system_ref': input_file['output'][line], 'informativeness': informativeness, 'quality': quality, 'naturalness': naturalness}, ignore_index=True)

filename += '_final'
if args.m:
    filename += '_med.tsv'
else:
    filename += '_avg.tsv'

save = new_file.to_csv(path_or_buf=filename, sep='\t', index=False)