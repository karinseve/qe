from argparse import ArgumentParser
import numpy as np
import re

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('data_file', type=str, help='Input file')
    ap.add_argument('-file', type=str, help='NEM or E2E dataset')
    args = ap.parse_args()

    input_file = args.data_file

    with open(input_file) as f:
        pearson_n = []
        pearson_q = []
        spearman_n = []
        spearman_q = []
        mae_n = []
        mae_q = []
        rmse_n = []
        rmse_q = []

        acc_q = []
        loss_q = []
        for line in f:
            if args.file == 'nem':
                match_pn = re.match('.+NATURALNESS Pearson correlation:\s(0\.[0-9]+)', line)
                if match_pn:
                    pearson_n.append(float(match_pn.group(1)))

                match_pq = re.match('.+QUALITY Pearson correlation:\s(0\.[0-9]+)', line)
                if match_pq:
                    pearson_q.append(float(match_pq.group(1)))

                match_sn = re.match('.+NATURALNESS Spearman correlation:\s(0\.[0-9]+)', line)
                if match_sn:
                    spearman_n.append(float(match_sn.group(1)))

                match_sq = re.match('.+QUALITY Spearman correlation:\s(0\.[0-9]+)', line)
                if match_sq:
                    spearman_q.append(float(match_sq.group(1)))

                match_mn = re.match('.+NATURALNESS MAE:\s(0\.[0-9]+)', line)
                if match_mn:
                    mae_n.append(float(match_mn.group(1)))

                match_mq = re.match('.+QUALITY MAE:\s(0\.[0-9]+)', line)
                if match_mq:
                    mae_q.append(float(match_mq.group(1)))

                match_rn = re.match('.+NATURALNESS.*RMSE:\s(1\.[0-9]+)', line)
                if match_rn:
                    rmse_n.append(float(match_rn.group(1)))

                match_rq = re.match('.+QUALITY.*RMSE:\s(1\.[0-9]+)', line)
                if match_rq:
                    rmse_q.append(float(match_rq.group(1)))

            elif args.file == 'e2e':
                match_aq = re.match('.+QUALITY Pairwise rank accuracy:\s(0\.[0-9]+)', line)
                if match_aq:
                    acc_q.append(float(match_aq.group(1)))
                match_lq = re.match('.+QUALITY.*avg:\s(0\.[0-9]+)', line)
                if match_lq and (float(match_lq.group(1)) != 0.0):
                    loss_q.append(float(match_lq.group(1)))


        if args.file == 'nem':
            # print(pearson_n)
            final_pearson = []
            for n, q in zip(pearson_n, pearson_q):
                # print(n,q,(n+q)/2)
                final_pearson.append((n+q)/2)
            final_pearson = np.array(final_pearson)
            print('Pearson:', np.mean(final_pearson))

            final_spearman = []
            for n, q in zip(spearman_n, spearman_q):
                # print(n,q,(n+q)/2)
                final_spearman.append((n+q)/2)
            final_spearman = np.array(final_spearman)
            print('Spearman:', np.mean(final_spearman))

            final_mae = []
            for n, q in zip(mae_n, mae_q):
                # print(n,q,(n+q)/2)
                final_mae.append((n+q)/2)
            final_mae = np.array(final_mae)
            print('MAE:', np.mean(final_mae))

            final_rmse = []
            for n, q in zip(rmse_n, rmse_q):
                # print(n,q,(n+q)/2)
                final_rmse.append((n+q)/2)
            final_rmse = np.array(final_rmse)
            print('RMSE:', np.mean(final_rmse))

        elif args.file == 'e2e':
            acc_q = np.array(acc_q)
            print('Accuracy: ', np.mean(acc_q))
            print(loss_q)
            loss_q = np.array(loss_q)
            print('Avg. loss:', np.mean(loss_q))